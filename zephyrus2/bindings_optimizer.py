'''
bindings_opt.py [<options>] <json description file> <configuration_file>
  Options:
    -o, --ofile: file where to save the output
    -v, --verbose
    -d, --dot: file where to save the graphical representation in dot
'''
__author__ = "Jacopo Mauro"
__copyright__ = "Copyright 2016, Jacopo Mauro"
__license__ = "ISC"
__version__ = "0.1"
__maintainer__ = "Jacopo Mauro"
__email__ = "mauro.jacopo@gmail.com"
__status__ = "Prototype"

import json
from subprocess import Popen
import sys, getopt
import os
import logging as log
import zephyrus2
import bind_preferences_translator
import psutil
import signal
from solver import Solver
from sets import Set
import shutil
import settings


DEVNULL = open(os.devnull, 'wb')

# If KEEP, don't delete temporary files.
KEEP = False

# List of the running solvers.
RUNNING_SOLVERS = []

# List of the temp files.
TMP_FILES = []


def usage():
  """Print usage"""
  print(__doc__)
  
def handler(signum = None, frame = None):
  """
  Handles termination signals.
  """
  log.critical('Signal handler called with signal' + str(signum))
  clean()
  sys.stdout.close()
  sys.exit(signum)

for sig in [signal.SIGTERM, signal.SIGINT, signal.SIGHUP, signal.SIGQUIT]:
  signal.signal(sig, handler)

def send_signal_solver(signal, solver):
  """
  Sends the specified signal to the solver process, and to all its children.
  """
  proc = solver.process
  if proc.poll() is None:
    for p in proc.children(recursive = True):
      try:
        p.send_signal(signal)
      except psutil.NoSuchProcess:
        pass
    try:
      proc.send_signal(signal)
    except psutil.NoSuchProcess:
      pass


  
def clean():
  """
  Utility for (possibly) cleaning temporary files and killing the solvers 
  processes at the end of the solving process (even when the termination is 
  forced externally).
  """
  global RUNNING_SOLVERS
  for solver in RUNNING_SOLVERS:
    solver.terminate()
  # Possibly remove temporary files.
  if not KEEP:
    settings.TMP_FILES_LOCK.acquire()
    for f in settings.TMP_FILES:
      if os.path.exists(f):
        os.remove(f)
    settings.TMP_FILES_LOCK.release()



def get_instances(data):
  """ Process the locations json entry to retrieve data """  
  inst_to_comp = {}
  inst_to_loc = {}
  counter = 1
  for i in data["locations"].keys():
    for j in data["locations"][i].keys():
      for k in data["locations"][i][j].keys():
        for l in range(int(data["locations"][i][j][k])):
          inst_to_comp[counter] = k
          inst_to_loc[counter] = (i,j,l)
          counter += 1
  return (inst_to_comp, inst_to_loc)

def get_bindings_num(data):  
  bindings_num = {}
  for i in data["bindings"]:
    bindings_num[(i["provider"],i["requirer"],i["port"])] = int(i["num"])
  return bindings_num

def generate_dzn(data, conf,
               inst_to_comp, inst_to_loc,
               int_to_port, port_to_int,
               int_to_comp, comp_to_int,
               int_to_loc, loc_to_int,
               mports,
               out):
  """Generate the dzn"""
  
  out.write("comps = 1.." + str(len(int_to_comp)) + ";\n")
  out.write("ports = 1.." + str(len(int_to_port)) + ";\n")
  out.write("multi_provide_ports = 1.." + str(len(mports)) + ";\n")
  out.write("insts = 1.." + str(len(inst_to_comp)) + ";\n")
  
  out.write("requirement_port_nums = ")
  temp = []
  for i in range(1,len(int_to_comp)+1):
    for j in range(1,len(int_to_port)+1):
      if int_to_port[j] in data["components"][int_to_comp[i]]["requires"]:
        temp.append(str(data["components"][int_to_comp[i]]["requires"][int_to_port[j]]))
      else:
        temp.append("0")
  zephyrus2.print_dzn_matrix(temp,len(int_to_port), out)
  
  out.write("provide_port_nums = ") 
  temp = {}
  for i in range(1,len(int_to_comp)+1):
    temp[i] = {}
    for j in range(1,len(mports)+1):
      temp[i][j] = "0"
  for i in mports.keys():    
      (_,x,y) = mports[i]
      temp[y][i] = str(x)             
  zephyrus2.print_dzn_matrix(
    [ temp[i][j] for i in range(1,len(int_to_comp)+1) for j in range(1,len(mports)+1)],
    len(mports), out)

  out.write("multi_provides = ")
  temp = []
  for i in range(1,len(mports)+1):
    for j in range(1,len(int_to_port)+1):
      if j in mports[i][0]:
        temp.append("true")
      else:
        temp.append("false")
  zephyrus2.print_dzn_matrix( temp, len(int_to_port), out)

  out.write("inst_comp = [")
  out.write(str(comp_to_int[inst_to_comp[1]]))
  for i in range(2,len(inst_to_comp)+1):
    out.write(", " + str(comp_to_int[inst_to_comp[i]]))
  out.write("];\n")
  
  bindings = get_bindings_num(conf)
  ls = []
  for i in range(len(comp_to_int)):
    for j in range(len(comp_to_int)):
      for k in range(len(port_to_int)):
        if (int_to_comp[i+1],int_to_comp[j+1],int_to_port[k+1]) in bindings:
          ls.append(bindings[(int_to_comp[i+1],int_to_comp[j+1],int_to_port[k+1])])
        else:
          ls.append(0)
  out.write("bindings_num_aux = " + str(ls) + ";\n")
  
  out.write("locations = ")
  out.write(str([loc_to_int[ (inst_to_loc[i+1][0],int(inst_to_loc[i+1][1])) ] for
                  i in range(len(inst_to_loc))]) + ";\n")                 


def print_solution(solution,
                 inst_to_comp, inst_to_loc,
                 int_to_port, port_to_int,
                 int_to_comp, comp_to_int,
                 zephyrus_configuration,
                 out_stream):
  """ Print a solution """
  lines = solution.split("\n")
  bindings = []
  for line in lines:
    if line.startswith("bindings|"):
      bindings = map(lambda x: (int)(x), line.split("|")[1][1:-1].split(","))
  
  res = []
  counter = 0
  for i in range(1,1+len(inst_to_comp)):
    for j in range(1,1+len(inst_to_comp)):
      for k in range(1,1+len(int_to_port)):
        if bindings[counter] != 0:
          res.append({ "prov_location" : inst_to_loc[i][0],
                      "prov_location_num" :  int(inst_to_loc[i][1]),
                      "prov_comp" : inst_to_comp[i],
                      "prov_comp_num" : inst_to_loc[i][2],
                      "req_location" : inst_to_loc[j][0],
                      "req_location_num" :  int(inst_to_loc[j][1]),
                      "req_comp" : inst_to_comp[j],
                      "req_comp_num" : inst_to_loc[j][2],
                      "port" : int_to_port[k] } )
        counter += 1

  data = { "optimized_bindings": res, "configuration": zephyrus_configuration }
  json.dump(data,out_stream)
  
def print_dot(solution,
                 inst_to_comp, inst_to_loc,
                 int_to_port, port_to_int,
                 int_to_comp, comp_to_int,
                 int_to_loc,loc_to_int,
                 out_stream):
  """ Print a solution in dot format """
  lines = solution.split("\n")
  bindings = []
  for line in lines:
    if line.startswith("bindings|"):
      bindings = map(lambda x: (int)(x), line.split("|")[1][1:-1].split(","))

  # get used ports
  used_prov = {}
  used_req = {}
  counter = 0
  for i in range(1,1+len(inst_to_comp)):
    for j in range(1,1+len(inst_to_comp)):
      for k in range(1,1+len(int_to_port)):
        if bindings[counter] != 0:
          if i in used_prov:
            used_prov[i][k] = 0
          else:
            used_prov[i] = {}
            used_prov[i][k] = 0
          if j in used_req:
            used_req[j][k] = 0
          else:
            used_req[j] = {}
            used_req[j][k] = 0
            
        counter += 1       
  
  out_stream.write("digraph Configuration {\n")
  out_stream.write("rankdir=LR;\n")
  for i in Set(inst_to_loc.values()):
    out_stream.write('subgraph cluster_' + str(i[0]).replace(".","_dot_")
         + "_" + str(i[1]) + ' {\n')
    out_stream.write('label = "' + str(i[0]) + "_" + str(i[1]) + '";\n')
    for j in inst_to_comp.keys():
      if inst_to_loc[j] == i:
        out_stream.write( "comp_" + str(j))
        out_stream.write(' [shape=box,label=<\n')
        out_stream.write('<table border="0" cellborder="1" cellspacing="0"><tr><td colspan="2">')
        out_stream.write( inst_to_comp[j])
        out_stream.write('</td></tr>\n')
        # provide ports
        if j in used_prov:
          out_stream.write('<tr><td><table border="0" cellborder="1" cellspacing="0" bgcolor="green">\n')
          for k in port_to_int.keys():
            if port_to_int[k] in used_prov[j]:
              out_stream.write('<tr><td port="')
              out_stream.write("prov_" + k)
              out_stream.write('">')
              out_stream.write(k)
              out_stream.write('</td></tr>\n')
          out_stream.write('</table></td></tr>\n')
        # require ports
        if j in used_req:
          out_stream.write('<tr><td><table border="0" cellborder="1" cellspacing="0" bgcolor="red">\n')
          for k in port_to_int.keys():
            if port_to_int[k] in used_req[j]:
              out_stream.write('<tr><td port="')
              out_stream.write("req_" +k)
              out_stream.write('">')
              out_stream.write(k)
              out_stream.write('</td></tr>\n')
          out_stream.write('</table></td></tr>\n')
        out_stream.write('</table>\n')
        out_stream.write('>];\n')
    out_stream.write('}\n')
    
  counter = 0
  for i in range(1,1+len(inst_to_comp)):
    for j in range(1,1+len(inst_to_comp)):
      for k in range(1,1+len(int_to_port)):
        if bindings[counter] != 0:
          out_stream.write("comp_" + str(i) + ":")
          out_stream.write("prov_" + int_to_port[k])
          out_stream.write(" -> ")
          out_stream.write("comp_" + str(j) + ":")
          out_stream.write("req_" + int_to_port[k])
          out_stream.write("\n")
        counter += 1       
  out_stream.write('}\n')
        
  
def main(argv):    
      
  output_file = ""
  dot_file = ""
  try:
    opts, args = getopt.getopt(argv,"ho:vkd:",["help","ofile=","verbose","keep","dot="])
  except getopt.GetoptError as err:
    print str(err)
    usage()
    sys.exit(1)
  for opt, arg in opts:
    if opt == '-h':
      usage()
      sys.exit()
    elif opt in ("-o", "--ofile"):
      output_file = arg
    elif opt in ("-k", "--keep"):
      global KEEP
      KEEP = True
    elif opt in ("-d", "--dot"):
      dot_file = arg
    elif opt in ("-v", "--verbose"):
      log.basicConfig(format="%(levelname)s: %(message)s", level=log.DEBUG)
      log.info("Verbose output.")

  if len(args) != 2:
    print "2 arguments are required"
    usage()
    sys.exit(1)
  
  input_file = args[0]
  target = args[1]
  
  input_file = os.path.abspath(input_file)
  target = os.path.abspath(target)
  
  out_stream = sys.stdout
  if output_file:
    out_stream = open(output_file, "w") 
   
  script_directory = os.path.dirname(os.path.realpath(__file__))
  
  dzn_file = settings.get_new_temp_file("dzn")
  
  global TMP_FILES
  TMP_FILES = [ dzn_file ]
    
  log.info("Parsing JSON file")
  try:
    data = settings.validate_input_json(zephyrus2.read_json(input_file))

    # if not binding preferences are given the default is to minimize local bindings
    if "bind preferences" not in data:
      data["bind preferences"] = [ "local" ]

    conf = zephyrus2.read_json(target)


    (int_to_port, port_to_int) = zephyrus2.get_ports(data)
    (int_to_comp,comp_to_int) = zephyrus2.get_comps(data)
    mports = zephyrus2.get_multiple_provs(data,comp_to_int,port_to_int)
    (int_to_loc,loc_to_int) = zephyrus2.get_locations(data)
    (inst_to_comp, inst_to_loc) = get_instances(conf)

    log.debug("instances mapping:")
    log.debug(str(inst_to_comp))
    log.debug("components ids:")
    log.debug(str(comp_to_int))
    log.debug("ports ids:")
    log.debug(str(port_to_int))
    log.debug("locations ids:")
    log.debug(str(loc_to_int))



    if len(inst_to_comp) > 0:
      log.info("Generating DZN")
      with open(dzn_file, 'w') as f:
        generate_dzn(data, conf,
                 inst_to_comp, inst_to_loc,
                 int_to_port, port_to_int,
                 int_to_comp, comp_to_int,
                 int_to_loc, loc_to_int,
                 mports,
                 f)

    log.info("Parsing preferences")
    pref_constraints = bind_preferences_translator.translate_preferences(
        data["bind preferences"],
        inst_to_comp, inst_to_loc,
        port_to_int,
        comp_to_int,
        loc_to_int)
    log.debug( "Metric\n" + pref_constraints)

    mzn_file = settings.get_new_temp_file("mzn")
    log.debug('Copy mzn program into ' + mzn_file)
    shutil.copyfile(script_directory + "/bindings_optimizer.mzn", mzn_file)
    log.info("Append metric")
    with open(mzn_file, 'a') as outfile:
      outfile.write(pref_constraints)

    log.info("Running solvers")
    global RUNNING_SOLVERS
    RUNNING_SOLVERS = [
  #     Solver("minizinc", script_directory + "/bindings_optimizer.mzn", dzn_file,
  #            'metric_satisfy', "gecode", options = ['--globals-dir', 'gecode','--flatzinc-cmd', 'fzn-gecode'])
  #     Solver("lex", script_directory + "/bindings_optimizer.mzn", dzn_file,
  #            '', "lex-gecode", options = ['--globals-dir', 'gecode', '--flatzinc-cmd', 'fzn-gecode']),
      Solver("lex", mzn_file, dzn_file,
             '', "lex-chuffed", options = ['--globals-dir', 'chuffed', '--flatzinc-cmd', 'fzn-chuffed'])
  #     Solver("smt", script_directory + "/bindings_optimizer.mzn", dzn_file,
  #            'metric_satisfy', "smt", options = [])

      ]

    solution = ""
    for i in RUNNING_SOLVERS:
      i.run()
      log.debug("Solver " + i.get_id() + " started")
      solution = i.get_solution()
      log.debug("****Solution")
      log.debug(solution)
      log.debug("****Solution")

    log.debug("Printing solution")
    print_solution(solution,
                   inst_to_comp, inst_to_loc,
                   int_to_port, port_to_int,
                   int_to_comp, comp_to_int,
                   conf,
                   out_stream)

    if dot_file:
      dot_out = open(dot_file, "w")
      log.debug("Writing dot file")
      print_dot(solution,
                   inst_to_comp, inst_to_loc,
                   int_to_port, port_to_int,
                   int_to_comp, comp_to_int,
                   int_to_loc,loc_to_int,
                   dot_out)

    log.info("Clean.")
    clean()
    log.info("Program Succesfully Ended")

  except ValueError as e:
    log.error("{}. Exiting".format(e))
    sys.exit(1)


if __name__ == "__main__":
  main(sys.argv[1:])