""" Module for translating the specification into minizinc constraints """
__author__ = "Jacopo Mauro"
__copyright__ = "Copyright 2016, Jacopo Mauro"
__license__ = "ISC"
__version__ = "0.1"
__maintainer__ = "Jacopo Mauro"
__email__ = "mauro.jacopo@gmail.com"
__status__ = "Prototype"

from antlr4 import *
from SpecificationGrammar.SpecificationGrammarLexer import SpecificationGrammarLexer
from SpecificationGrammar.SpecificationGrammarParser import SpecificationGrammarParser
from SpecificationGrammar.SpecificationGrammarVisitor import SpecificationGrammarVisitor

import re
#import settings

class SpecificationParsingException(Exception):
  
  def __init__(self,value):
    self.value = value
  
  def __str__(self):
    return repr(self.value)

class MyVisitor(SpecificationGrammarVisitor):

  def __init__(self, comp_to_int, res_to_int, loc_to_int ):
    """
    init some variables
    """    
    SpecificationGrammarVisitor.__init__(self)
    self.comp_to_int = comp_to_int
    self.res_to_int = res_to_int
    self.loc_to_int = loc_to_int
    "true if query contains ID[INT].ID"
    self.constraints_comp_in_loc = False
    "true if query contains _[_]"
    self.constraints_over_loc = False
    self.preference = []
    
  def getDCset(self,name):
    "given a name finds the set of DC of the component types"
    s = ''
    if (name,0) in self.loc_to_int:
        counter = 1
        while (name,counter) in self.loc_to_int:
          counter += 1
        s += str(self.loc_to_int[(name,0)]) + ".." + \
          str(self.loc_to_int[(name,counter-1)])
    else:
      raise SpecificationParsingException("\"" + name +
        "\" is not a valid location name")
    return s
  
  def getDCsets(self,pattern):
    matches = set([]) 
    for (x,_) in self.loc_to_int:
      if re.match(pattern,x):
        matches.add(x)
    ls = list(matches)
    if not ls:
      matches = set([]) 
      for (x,_) in self.loc_to_int:
        matches.add(x)
      raise SpecificationParsingException("\"" + pattern +
        "\" does not match with any DC: " + str(matches))
    else:
      s = self.getDCset(ls[0])
      for i in ls[1:]:
        s += ' union ' + self.getDCset(i)
      return s

  def defaultResult(self):
    return ""
  
  def visitTerminal(self, node):
    
    switcher = {
      'or' : "\\/",
      'and': '/\\',
      'impl': "->",
      'iff' : "<->"           
    }
    txt = node.getText()
    if txt in switcher:
      return switcher[txt]
    else:
      return txt
  
  def aggregateResult(self, aggregate, nextResult):
    return aggregate + nextResult
  
  def visitErrorNode(self, node):
    token = node.getSymbol()    
    raise SpecificationParsingException("Erroneous Node at line "  +
            str(token.line) + ", column " + str(token.column) + ": '" + 
            str(token.text) + "'"  )
  
  def visitStatement(self, ctx):
    n = ctx.getChildCount()
    for i in range(2,n,2):
      ctx.getChild(i).accept(self)
    return ctx.getChild(0).accept(self)
  
  def visitApreferenceCost(self, ctx):
    #self.preference.append("sum(l in locations) ( used_locations[l] * costs[l] )")
    self.preference.append("total_cost")
    return ""
  
  def visitApreferenceExpr(self, ctx):
    self.preference.append(ctx.getChild(0).accept(self))
    return ""
  
  def visitVariable(self, ctx):
    return ctx.getChild(0).accept(self)[1:]
  
#   def visitAformulaBrackets(self, ctx):
#     return "( " +  ctx.getChild(1).accept(self) + " )"
  
  def visitAexprQuantifier(self, ctx):
    s = ctx.getChild(0).accept(self)
    s += "(" + ctx.getChild(1).accept(self) + " in "
    t = ctx.getChild(3).accept(self)
    if t == "components": 
      s += "comps )( "
    elif t == "locations":
      s += "locations )( "
    else:
      s += self.getDCsets(t[1:-1]) + ")( "
    s += ctx.getChild(5).accept(self) + " )"
    return s

#   def visitAexprBrackets(self, ctx):
#     return "( " +  ctx.getChild(1).accept(self) + " )"
    
  def visitAexprId(self, ctx):
    comp = ctx.getChild(0).accept(self)
    if comp in self.comp_to_int:
      return "sum ( l in locations) ( comp_locations[l," + \
        str(self.comp_to_int[comp]) + "])"
    else:
      raise SpecificationParsingException("\"" + comp +
        "\" is not a valid component name")
    return ""
    
  def visitAexprIDLoc(self, ctx):
    loc = ctx.getChild(0).accept(self)
    num = int(ctx.getChild(2).accept(self))
    obj = ctx.getChild(5).accept(self)
    if (loc,num) in self.loc_to_int:
      if obj in self.comp_to_int:
        self.constraints_comp_in_loc = True
        self.constraints_over_loc = True
        return "comp_locations[" + str(self.loc_to_int[(loc,num)]) + "," + \
          str(self.comp_to_int[obj]) + "]"
      elif obj in self.res_to_int:
        # symmetry constraint ok because they trigger only if loc has same resources
        return "resource_provisions[ " + str(self.loc_to_int[(loc,num)]) + "," + \
          str(self.res_to_int[obj]) + "]"
      else:
        raise SpecificationParsingException("\"" + obj +
          "\" is not a valid component or resource name")
    else:
      raise SpecificationParsingException(loc + "[" + str(num) +
         "] is not a valid location")
    return "" 
  
  def visitAexprVariable(self, ctx):
    return "sum ( l in locations) ( comp_locations[l," + \
        ctx.getChild(0).accept(self) + "])"


   
  def visitAexprVariableId(self, ctx):
    var = ctx.getChild(0).accept(self)
    obj = ctx.getChild(2).accept(self)
    if obj in self.comp_to_int:
      self.constraints_over_loc = True
      return "comp_locations[" + var + "," + \
        str(self.comp_to_int[obj]) + "]"
    elif obj in self.res_to_int:
      # symmetry constraint ok because they trigger only if loc has same resources
      return "resource_provisions[ " + var + "," + \
        str(self.res_to_int[obj]) + "]"
    else:
      raise SpecificationParsingException("\"" + obj +
        "\" is not a valid component or resource name")
    return ""


  def visitAexprLocVariable(self, ctx):
    loc = ctx.getChild(0).accept(self)
    num = int(ctx.getChild(2).accept(self))
    var = ctx.getChild(5).accept(self)
    if (loc,num) in self.loc_to_int:
      self.constraints_comp_in_loc = True
      self.constraints_over_loc = True
      return "comp_locations[" + str(self.loc_to_int[(loc,num)]) + "," + \
          var + "]"
    else:
      raise SpecificationParsingException(loc + "[" + str(num) +
         "] is not a valid location")
    return "" 


  def visitAexprVariablevariable(self, ctx):
    self.constraints_over_loc = True
    return "comp_locations[" + ctx.getChild(0).accept(self) + "," + \
          ctx.getChild(2).accept(self) + "]"


  def visitAexprSum(self, ctx):
    var = ctx.getChild(1).accept(self)
    ty = ctx.getChild(3).accept(self)
    expr = ctx.getChild(5).accept(self)
    s = "sum(" + var + " in "
    if ty == "components": 
      s += "comps )( "
    elif ty == "locations":
      s += "locations )( "
    else:
      s += self.getDCsets(ty[1:-1]) + ")( "
    s += expr + " )"
    return s


def translate_specification(input_stream, comp_to_int, res_to_int, loc_to_int,
      symmetry_breaking):
  lexer = SpecificationGrammarLexer(input_stream)
  stream = CommonTokenStream(lexer)
  parser = SpecificationGrammarParser(stream)
  tree = parser.statement()
  visitor = MyVisitor(comp_to_int, res_to_int, loc_to_int)
  s = "constraint " + visitor.visit(tree) + ";\n"
  if symmetry_breaking:
    s += 'constraint symmetry_breaking_constraints(true);\n'
  
  if visitor.preference == []: # default preference -> minimize costs
    s += "array[1..1] of var int: obj_array;\n"
    s += "constraint obj_array[1] = sum(l in locations) ( used_locations[l] * costs[l] );"
  else:
    s += "array[1.." + str(len(visitor.preference)) + "] of var int: obj_array;\n"
    for i in range(len(visitor.preference)):
      s += "constraint obj_array[" + str(i+1) + "] = " +  visitor.preference[i] + ";\n"
  
  return (s)