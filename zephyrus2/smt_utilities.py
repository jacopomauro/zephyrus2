#!/usr/bin/python
"""
Module to translate the stmlib 1 into 2 for the instances
generated using the fzn2smt tool
"""

import re
import sys
import settings
import uuid
import logging as log
from subprocess import Popen,PIPE
from doctest import SKIP


def findMatch(r, src, flags = 0):
	g = re.search(r, src, flags)
	if g == None:
		log.error("Did not find a match for \"%s\"." % r)
		raise Exception( "Did not find a match for " + r)
	return g

def enumerate(r, src):
	return re.findall(r, src)

def replace(formula, f, to, replacedVars, bools, tvars):
	if f.startswith("obj"): return formula,False
	if f in ["true","false"]: f,to = to,f
	formula = formula.replace(f + " ", to + " ").replace(f + ")", to + ")").replace("\t\t(= %s %s)\n" % (to,to), "")
	if f in bools: bools.remove(f)
	else: tvars.pop([x[0] for x in tvars].index(f))
	replacedVars[f] = to
	return formula,True

def collectUpperBounds(formula):
	progress = True
	while progress:
		progress = False
		d = {}
		for v in re.finditer('\t\t\(<= ([a-zA-Z][a-zA-Z0-9_]+) ([0-9]+)\)\n', formula):
			d[v.group(1)] = int(v.group(2))
		for v in re.finditer('\t\t\(<= ([a-zA-Z][a-zA-Z0-9_]+) ([a-zA-Z][a-zA-Z0-9_]+)\)\n', formula):
			v1,v2 = v.group(1),v.group(2)
			if not v1 in d: continue
			if not v2 in d: continue
			#print("%s <= %s" % (v1,v2))
			if d[v1] == d[v2]:
				#print("Removing (<= %s %s)" % (v1,d[v1]))
				formula = formula.replace("\t\t(<= %s %s)\n" % (v1,d[v1]), "")
				progress = True
			if progress: break
	return formula

def performSimplification(formula,replacedVars, bools, tvars):
	reEquality = re.compile("\t\t\(= ([^\)]+)\)\n")
	reVarEq = re.compile("([a-zA-Z][a-zA-Z0-9_]+) ([a-zA-Z][a-zA-Z0-9_]+)")
	reAndEq = re.compile("(\(and  [a-zA-Z][a-zA-Z0-9_]+ [a-zA-Z][a-zA-Z0-9_]+\)) ([a-zA-Z][a-zA-Z0-9_]+)")
	progress = True
	while progress:
		progress = False
		for m in reEquality.finditer(formula):
			m = m.group(1)
			m1 = reVarEq.match(m)
			if m1 != None:
				formula,progress = replace(formula, m1.group(2), m1.group(1), replacedVars, bools, tvars)
				if progress: break
			m1 = reAndEq.match(m)
			if m1 != None:
				formula,progress = replace(formula, m1.group(2), m1.group(1), replacedVars, bools, tvars)
				if progress: break
	return formula

def translateSMTFormat(in_file,out_file):	
	fzn = open(in_file).read()

	# change X_INTRODUCED_NNN_ variables name into X_INTRODUCED_NNN
	# otherwise the fzn2smt does not work

	fzn = re.sub('X_INTRODUCED_([0-9]+)_','X_INTRODUCED_\g<1>',fzn)

	inttimes = []
	replacedVars = {}

	for match in re.finditer(
		'constraint int_times\(([a-zA-Z0-9_]+),([a-zA-Z0-9_]+),([a-zA-Z0-9_]+)\)[^;]+;', fzn):
		inttimes.append((match.group(1),match.group(2),match.group(3)))
		fzn = fzn.replace(match.group(0), "")
	
	new_fzn_file = settings.get_new_temp_file('fzn')
	smtlib1_file = settings.get_new_temp_file('smt')
	open(new_fzn_file, "w").write(fzn)

	# convert instance into smtlib by using fzn2smt
	cmd = ['java', 'fzn2smt', '-ne']
	cmd += [ '-os', smtlib1_file]
	cmd += [ '-i', new_fzn_file]
	log.info("Running command: "  + str.join(' ',cmd))
	proc = Popen( cmd, stdout=PIPE, stderr=PIPE )
	out, err = proc.communicate()
	log.debug('Stdout')
	log.debug(out)
	log.debug('Stderr')
	log.debug(err)
	log.debug('Return code fzn: ' + str(proc.returncode))
	
	src = open(smtlib1_file ).read()

	logic = findMatch(':logic (QF_[A-Z]+)', src).group(1)
	bools = enumerate('\(([a-zA-Z0-9_]+)\)', findMatch(':extrapreds \( ([^\n]+)\)', src).group(1))
	tvars = enumerate('\(([a-zA-Z0-9_]+) ([a-zA-Z]+)\)', findMatch(':extrafuns \( ([^\n]+)\)', src).group(1))
	formula = findMatch(':formula (.*)', src, re.DOTALL).group(1)
	obj = sorted(filter(lambda x: x[0].startswith("obj"), tvars), key = lambda x: x[0])

	formula = "(assert " + formula
	for i in inttimes:
		formula = formula + "(assert (= (* %s %s) %s))\n" % i
	formula = formula.replace("iff", "=").replace("implies", "=>").replace("~", "-")

	formula = performSimplification(formula,replacedVars, bools, tvars)
	for v in re.finditer('\(>= ([a-zA-Z][a-zA-Z0-9_]+) ([0-9]+)\)\s*\(<= \\1 \\2\)', formula):
		# look for coinciding lower and upper bounds
		formula = formula.replace(v.group(0), "(= %s %s)" % (v.group(1), v.group(2)))
	for v in re.finditer('\t\t\(= ([a-zA-Z][a-zA-Z0-9_]+) ([0-9]+)\)\n', formula):
		# look for variables assigned to a constant
		formula,dummy = replace(formula, v.group(1), v.group(2), replacedVars, bools, tvars)

	formula = performSimplification(formula,replacedVars, bools, tvars)
	for v in re.finditer('\(assert \(= \(\* ([a-zA-Z][a-zA-Z0-9_]+) ([a-zA-Z][a-zA-Z0-9_]+)\) 0\)\)', formula):
		# expand (= (* x y) 0)
		formula = formula.replace(v.group(0), "%s\n(assert (or (= %s 0) (= %s 0)))" % (v.group(0), v.group(1), v.group(2)))
	formula = performSimplification(formula,replacedVars, bools, tvars)
	formula = collectUpperBounds(formula)
	formula = performSimplification(formula,replacedVars, bools, tvars)
	
	out = open(out_file, "w")
	
	out.write("(set-logic QF_NIA)\n")
	for b in bools:
		out.write("(declare-fun %s () Bool)\n" % b)
	for t in tvars:
		out.write("(declare-fun %s () %s)\n" % (t[0], t[1]))
	out.write(formula + "\n")
	for o in obj:
		out.write("(minimize %s)\n" % o[0])
	out.write("(check-sat)\n")
	out.write("(get-model)\n")
# 	for r in replacedVars:
# 		out.write("(echo \"(removed-var %s = %s)\")\n" % (r, replacedVars[r]))
	return replacedVars

def reconstruct_output_array(name,vs):
		d = { int( re.match('[a-zA-Z0-9_]*_([0-9]+)_',key).group(1)) : value
				for key, value in vs.iteritems() if key.startswith(name)}
		return [ value for (key, value) in sorted(d.items())]

def getVariableValues(src):
	return { key: int(value) for (key, value) in enumerate(
		'\(define-fun ([a-zA-Z0-9_]+) \(\) Int\n\s+([0-9]+)',src) }
	
def restoreValuesofRemovedVariables(replacedVars,vs):
	var_assigned = False
	while not var_assigned:
		var_assigned = True
		for k in replacedVars.keys():
			try:
				int(replacedVars[k])
				vs[k] = int(replacedVars[k])
				del replacedVars[k]
				var_assigned = False
			except ValueError:
				if replacedVars[k] in vs:
					vs[k] = vs[replacedVars[k]]
					del replacedVars[k]
					var_assigned = False
				elif replacedVars[k] == 'true':
					vs[k] = 1
					del replacedVars[k]
				elif replacedVars[k] == 'false':
					vs[k] = 0
					del replacedVars[k]
	if replacedVars:
		log.debug(str(replacedVars))
		raise Exception("Variables are not assigned by the smt solver")
	

def parseOutput(src, replacedVars):

	vs = getVariableValues(src)
	log.debug('Variables assigned by the SMT')
	log.debug(str(vs))
	restoreValuesofRemovedVariables(replacedVars,vs)
	
	s = ''
	if src.startswith('sat'):
		s += 'components' + str(reconstruct_output_array('comps_num',vs))	+ '\n'
		s += 'bindings|' + str(reconstruct_output_array('bindings',vs))	+ '\n'
		s += 'comp_locations|' + str(reconstruct_output_array('comp_locations',vs))	+ '\n'
		s += 'used_locations|' + str(reconstruct_output_array('used_locations',vs))	+ '\n'
		s += '###OBJVAR|' + str(reconstruct_output_array('obj_array',vs))	+ '\n'
		s += '----------\n'
		s += '==========\n'
	return s
	
