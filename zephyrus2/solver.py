""" Module for better handling the execution of a solver """
__author__ = "Jacopo Mauro"
__copyright__ = "Copyright 2016, Jacopo Mauro"
__license__ = "ISC"
__version__ = "0.1"
__maintainer__ = "Jacopo Mauro"
__email__ = "mauro.jacopo@gmail.com"
__status__ = "Prototype"


from subprocess import Popen,PIPE
#import fcntl
import settings
import os
import logging as log
import shutil
import re
import smt_utilities
import threading
import psutil
import signal


def read_lines(proc):
  """
  Returns the stream of the read lines of a process.
  """
  try:
    return proc.stdout.read()
  except:
    return ''


def get_objs(s):
  """
  Get the list of the obj vars extracted from a solution
  """
  return re.findall('###OBJVAR\|(.*)\n',s)[-1][1:-1].split(",")

              
def get_last_solution(buf):
  lines = buf.split("\n")
  lines.reverse()
  buf = ""
  sol = ""
  end = False
  
  find = False
  for line in lines:
    if line.startswith("=========="):
      end = True
    if not find:
      if line.startswith("----------"):
        find = True
      else:
        buf += line + "\n"
    else:
      if line.startswith("----------"):
        break
      else:
        sol += line + "\n"
  return (sol,buf,end)
  


class Solver (threading.Thread):
  
  # Contains the buffer having partial solutions
  buf = ""
  
  # Object of class psutil.Popen referring to the solving process.
  process = None
  
  # True if solver has explored the entire search space
  ended = False
  
  # Stderr of the process
  err = None
  
  # Because true if there is a request to terminate the solver
  terminated = False
  
  def __init__(self, solver, mzn_file, dzn_file, metric_file, id_string,
                options = [], search_annotation = ""):
    threading.Thread.__init__(self)
    self.solver = solver
    # additional option to pass to the solver
    self.options = options
    self.mzn_file = mzn_file
    self.dzn_file = dzn_file
    self.metric_file = metric_file
    self.id_string = id_string
    self.search_annotation = search_annotation
  
  
  def terminate(self):
    """
    Terminate the process if it is running and set the variable that variable
    that the thread should terminate
    """
    if not self.ended:
      log.debug( 'Termination for solver ' + self.id_string + " received")
      self.terminated = True
    if self.process is not None and self.process.poll() is None:
      log.debug('Kill process ' + str(self.process.pid))
      for p in self.process.children(recursive = True):
        try:
          p.send_signal(signal.SIGTERM)
        except psutil.NoSuchProcess:
          pass
      try:
        self.process.send_signal(signal.SIGTERM)
      except psutil.NoSuchProcess:
        pass
  
  
  def run_command(self, cmd):
    """
    Run the given program. When terminates raise an Exception
    """
    cmd_string = "'"  + str.join(' ',cmd) + "'"
    log.info("Running command "  + cmd_string)
    self.process = psutil.Popen( cmd, stdout=PIPE, stderr=PIPE )
      # For non-blocking read.
#       fd = self.process.stdout.fileno()
#       fl = fcntl.fcntl(fd, fcntl.F_GETFL)
#       fcntl.fcntl(fd, fcntl.F_SETFL, fl | os.O_NONBLOCK)    
    out, err = self.process.communicate()
    log.debug('Stdout of ' + cmd_string)
    log.debug(out)
    log.debug('Stderr of ' + cmd_string)
    log.debug(err)
    log.debug('Return code of ' + cmd_string + ': ' + str(self.process.returncode))
    if self.terminated:
      raise Exception('Tread terminated') 
    return out,err
    
  
  def run(self):
    try:
      # string for containing the out and err stream of the process
      
      script_directory = os.path.dirname(os.path.realpath(__file__))
      mzn_file = self.mzn_file
      if self.metric_file:
        mzn_file = settings.get_new_temp_file("mzn")
        log.debug('Copy mzn program into ' + mzn_file)
        shutil.copyfile(self.mzn_file, mzn_file)  
        log.info("Append metric")
        with open(script_directory + '/' + self.metric_file + '.mzn') as infile:
          with open(mzn_file, 'a') as outfile:
            for i in infile:
              outfile.write(i) 
      
      # smt solver
      if self.solver == "smt":
        fzn_file = settings.get_new_temp_file('fzn')
        smtlib2_file = settings.get_new_temp_file('smt2')
        
        # generate the fzn file
        cmd = ['mzn2fzn']
        cmd += [ "--no-output-ozn"]
        cmd += [ "--output-to-file", fzn_file]
        cmd += [ mzn_file, self.dzn_file ]
        out, err = self.run_command(cmd)
        self.but = out 
        
        if re.search('Warning: model inconsistency detected',err):
          self.buf = '==========\n' # fzn converter finds problem unsat
          self.process.returncode = 0 # set return code of mzn2fzn to 0
        elif self.process.returncode == 0:   
          # convert instance into smtlib2
          try:
            replace_vars = smt_utilities.translateSMTFormat(fzn_file,smtlib2_file)
          except Exception as e:
            raise Exception('Exception in translating the fzn into smt: ' + str(e))
            
          # execute smt solver
          cmd = ['z3','-smt2', smtlib2_file]
          out, err = self.run_command(cmd)
            
          if out.startswith('unsat'):
            self.buf = '==========\n'
            self.process.returncode = 0 # set return code to 0 (smt returns with 1)
          else:
            # parse output and add it into self.buf
            self.buf = smt_utilities.parseOutput(out, replace_vars)
  
      # lexicographic minimize
      elif self.solver == "lex":
        mzn_file_opt = settings.get_new_temp_file("mzn")
        log.debug('Copy mzn program into ' + mzn_file_opt)
        shutil.copyfile(self.mzn_file, mzn_file_opt)  
        log.info("Append metric")
        with open(mzn_file_opt, 'a') as outfile:
          #outfile.write("solve minimize(obj_array[1]);\n")
          
          outfile.write("solve " + self.search_annotation + 
            " minimize obj_array[1];")
        
        for i in range(len(self.options)):
          if self.options[i] == '--solver':
            self.options[i] = '--flatzinc-cmd'
          
        cmd = ['minizinc'] + self.options + [ mzn_file_opt, self.dzn_file]
        out, err = self.run_command(cmd)
            
        if re.search('=====UNSATISFIABLE=====', out):
          self.buf = '==========\n'
        else:
          try:
            obj = get_objs(out)
            log.debug("Find new solution with obj_array = " + str(obj))
          except Exception as e:
            # the solver can be bugged -> no unsat and no solution
            raise Exception('Exception in extracting the obj value: ' + str(e))

          self.buf = out
          for i in range(1,len(obj)):
            # allow the printing of the found solution
            out.replace('==========','')
            self.buf = out
                
            mzn_file_opt = settings.get_new_temp_file("mzn")
            log.debug('Copy mzn program into ' + mzn_file_opt)
            shutil.copyfile(self.mzn_file, mzn_file_opt)  
            log.info("Append metric")
            with open(mzn_file_opt, 'a') as outfile:
              log.debug('Add: solve minimize(obj_array[' + str(i+1) + '])')
              outfile.write('solve ' + self.search_annotation +
                ' minimize(obj_array[' + str(i+1) + ']);\n')
              for j in range(i):
                log.debug('Add: constraint obj_array[' + str(j+1) + '] = ' +
                         obj[j] + '; \n')
                outfile.write('constraint obj_array[' + str(j+1) + '] = ')
                outfile.write( obj[j] + '; \n')
                
            cmd = ['minizinc'] + self.options + [ mzn_file_opt, self.dzn_file]
            out, err = self.run_command(cmd)  
                
            try:
              obj = get_objs(out)
              log.debug("Find new possibly better solution with obj_array = " + str(obj))
            except Exception as e:
              # the solver can be bugged -> no unsat and no solution
              raise Exception('Exception in extracting the obj value: ' + str(e))
            self.buf = out

    except Exception as e: # of something happens or if the solver is terminated
      log.debug('Thread Exception in solver ' + self.get_id() + ': ' + str(e))
      if self.process.returncode == 0:
        self.process.returncode = 1
    self.ended = True # solver has ended its computation 

      
  def get_solution(self):
#     self.buf += read_lines(self.process)
    # Sometimes not all the lines are read from solver output.
#     if (self.process is not None) and (self.process.poll() is not None):
#       self.buf += read_lines(self.process)
    sol, self.buf, _ = get_last_solution(self.buf) 
    return sol

  
  def is_ended(self):
    return self.ended
  

  def get_id(self):
    return self.id_string