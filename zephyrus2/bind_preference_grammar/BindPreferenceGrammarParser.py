# Generated from BindPreferenceGrammar.g4 by ANTLR 4.7
# encoding: utf-8
from __future__ import print_function
from antlr4 import *
from io import StringIO
import sys

def serializedATN():
    with StringIO() as buf:
        buf.write(u"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3")
        buf.write(u")\u009b\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t")
        buf.write(u"\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r")
        buf.write(u"\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4")
        buf.write(u"\23\t\23\4\24\t\24\3\2\3\2\3\2\3\3\3\3\5\3.\n\3\3\4\3")
        buf.write(u"\4\3\4\3\4\7\4\64\n\4\f\4\16\4\67\13\4\3\5\5\5:\n\5\3")
        buf.write(u"\5\3\5\3\6\3\6\5\6@\n\6\3\7\3\7\3\7\3\7\5\7F\n\7\3\b")
        buf.write(u"\3\b\3\b\3\b\7\bL\n\b\f\b\16\bO\13\b\3\t\3\t\3\t\3\t")
        buf.write(u"\3\t\5\tV\n\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t")
        buf.write(u"\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\5\tl\n\t\3\t")
        buf.write(u"\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\5\tz\n\t")
        buf.write(u"\3\n\3\n\3\n\5\n\177\n\n\3\13\3\13\3\13\3\13\5\13\u0085")
        buf.write(u"\n\13\3\f\3\f\5\f\u0089\n\f\3\r\3\r\3\16\3\16\3\17\3")
        buf.write(u"\17\3\20\3\20\3\21\3\21\3\22\3\22\3\23\3\23\3\24\3\24")
        buf.write(u"\3\24\2\2\25\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 ")
        buf.write(u"\"$&\2\7\3\2\30\31\4\2\21\22\26\27\3\2\"$\3\2\34!\3\2")
        buf.write(u"\24\25\2\u0098\2(\3\2\2\2\4-\3\2\2\2\6/\3\2\2\2\b9\3")
        buf.write(u"\2\2\2\n?\3\2\2\2\fA\3\2\2\2\16G\3\2\2\2\20y\3\2\2\2")
        buf.write(u"\22~\3\2\2\2\24\u0080\3\2\2\2\26\u0088\3\2\2\2\30\u008a")
        buf.write(u"\3\2\2\2\32\u008c\3\2\2\2\34\u008e\3\2\2\2\36\u0090\3")
        buf.write(u"\2\2\2 \u0092\3\2\2\2\"\u0094\3\2\2\2$\u0096\3\2\2\2")
        buf.write(u"&\u0098\3\2\2\2()\5\4\3\2)*\7\2\2\3*\3\3\2\2\2+.\7\3")
        buf.write(u"\2\2,.\5\16\b\2-+\3\2\2\2-,\3\2\2\2.\5\3\2\2\2/\65\5")
        buf.write(u"\b\5\2\60\61\5\30\r\2\61\62\5\b\5\2\62\64\3\2\2\2\63")
        buf.write(u"\60\3\2\2\2\64\67\3\2\2\2\65\63\3\2\2\2\65\66\3\2\2\2")
        buf.write(u"\66\7\3\2\2\2\67\65\3\2\2\28:\5 \21\298\3\2\2\29:\3\2")
        buf.write(u"\2\2:;\3\2\2\2;<\5\n\6\2<\t\3\2\2\2=@\5\"\22\2>@\5\f")
        buf.write(u"\7\2?=\3\2\2\2?>\3\2\2\2@\13\3\2\2\2AE\5\16\b\2BC\5\36")
        buf.write(u"\20\2CD\5\16\b\2DF\3\2\2\2EB\3\2\2\2EF\3\2\2\2F\r\3\2")
        buf.write(u"\2\2GM\5\20\t\2HI\5\32\16\2IJ\5\20\t\2JL\3\2\2\2KH\3")
        buf.write(u"\2\2\2LO\3\2\2\2MK\3\2\2\2MN\3\2\2\2N\17\3\2\2\2OM\3")
        buf.write(u"\2\2\2PQ\t\2\2\2QU\5$\23\2RS\7\4\2\2ST\7\5\2\2TV\5&\24")
        buf.write(u"\2UR\3\2\2\2UV\3\2\2\2VW\3\2\2\2WX\7\6\2\2XY\5\22\n\2")
        buf.write(u"YZ\7\7\2\2Z[\5\6\4\2[z\3\2\2\2\\z\7(\2\2]^\7\b\2\2^_")
        buf.write(u"\7\t\2\2_`\5$\23\2`a\7\n\2\2ab\5$\23\2bc\7\n\2\2cd\5")
        buf.write(u"\26\f\2de\7\13\2\2ez\3\2\2\2fg\7\32\2\2gk\5$\23\2hi\7")
        buf.write(u"\4\2\2ij\7\5\2\2jl\5&\24\2kh\3\2\2\2kl\3\2\2\2lm\3\2")
        buf.write(u"\2\2mn\7\6\2\2no\5\22\n\2op\7\7\2\2pq\5\16\b\2qz\3\2")
        buf.write(u"\2\2rs\5\34\17\2st\5\16\b\2tz\3\2\2\2uv\7\t\2\2vw\5\6")
        buf.write(u"\4\2wx\7\13\2\2xz\3\2\2\2yP\3\2\2\2y\\\3\2\2\2y]\3\2")
        buf.write(u"\2\2yf\3\2\2\2yr\3\2\2\2yu\3\2\2\2z\21\3\2\2\2{\177\7")
        buf.write(u"\f\2\2|\177\7\r\2\2}\177\5&\24\2~{\3\2\2\2~|\3\2\2\2")
        buf.write(u"~}\3\2\2\2\177\23\3\2\2\2\u0080\u0084\7\'\2\2\u0081\u0082")
        buf.write(u"\7\16\2\2\u0082\u0083\7(\2\2\u0083\u0085\7\17\2\2\u0084")
        buf.write(u"\u0081\3\2\2\2\u0084\u0085\3\2\2\2\u0085\25\3\2\2\2\u0086")
        buf.write(u"\u0089\7\'\2\2\u0087\u0089\5$\23\2\u0088\u0086\3\2\2")
        buf.write(u"\2\u0088\u0087\3\2\2\2\u0089\27\3\2\2\2\u008a\u008b\t")
        buf.write(u"\3\2\2\u008b\31\3\2\2\2\u008c\u008d\t\4\2\2\u008d\33")
        buf.write(u"\3\2\2\2\u008e\u008f\7%\2\2\u008f\35\3\2\2\2\u0090\u0091")
        buf.write(u"\t\5\2\2\u0091\37\3\2\2\2\u0092\u0093\7\23\2\2\u0093")
        buf.write(u"!\3\2\2\2\u0094\u0095\t\6\2\2\u0095#\3\2\2\2\u0096\u0097")
        buf.write(u"\7&\2\2\u0097%\3\2\2\2\u0098\u0099\7\20\2\2\u0099\'\3")
        buf.write(u"\2\2\2\16-\659?EMUky~\u0084\u0088")
        return buf.getvalue()


class BindPreferenceGrammarParser ( Parser ):

    grammarFileName = "BindPreferenceGrammar.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ u"<INVALID>", u"'local'", u"'of'", u"'type'", u"'in'", 
                     u"':'", u"'bind'", u"'('", u"','", u"')'", u"'ports'", 
                     u"'locations'", u"'['", u"']'", u"<INVALID>", u"'and'", 
                     u"'or'", u"'not'", u"'true'", u"'false'", u"'impl'", 
                     u"'iff'", u"'exists'", u"'forall'", u"'sum'", u"'cost'", 
                     u"'<='", u"'='", u"'>='", u"'<'", u"'>'", u"'!='", 
                     u"'+'", u"'-'", u"'*'", u"'abs'" ]

    symbolicNames = [ u"<INVALID>", u"<INVALID>", u"<INVALID>", u"<INVALID>", 
                      u"<INVALID>", u"<INVALID>", u"<INVALID>", u"<INVALID>", 
                      u"<INVALID>", u"<INVALID>", u"<INVALID>", u"<INVALID>", 
                      u"<INVALID>", u"<INVALID>", u"RE", u"AND", u"OR", 
                      u"NOT", u"TRUE", u"FALSE", u"IMPL", u"IFF", u"EXISTS", 
                      u"FORALL", u"SUM", u"COST", u"LEQ", u"EQ", u"GEQ", 
                      u"LT", u"GT", u"NEQ", u"PLUS", u"MINUS", u"TIMES", 
                      u"ABS", u"VARIABLE", u"ID", u"INT", u"WS" ]

    RULE_statement = 0
    RULE_preference = 1
    RULE_b_expr = 2
    RULE_b_term = 3
    RULE_b_factor = 4
    RULE_relation = 5
    RULE_expr = 6
    RULE_term = 7
    RULE_typeV = 8
    RULE_locs = 9
    RULE_var_or_port = 10
    RULE_bool_binary_op = 11
    RULE_arith_binary_op = 12
    RULE_arith_unary_op = 13
    RULE_comparison_op = 14
    RULE_unaryOp = 15
    RULE_boolFact = 16
    RULE_variable = 17
    RULE_re = 18

    ruleNames =  [ u"statement", u"preference", u"b_expr", u"b_term", u"b_factor", 
                   u"relation", u"expr", u"term", u"typeV", u"locs", u"var_or_port", 
                   u"bool_binary_op", u"arith_binary_op", u"arith_unary_op", 
                   u"comparison_op", u"unaryOp", u"boolFact", u"variable", 
                   u"re" ]

    EOF = Token.EOF
    T__0=1
    T__1=2
    T__2=3
    T__3=4
    T__4=5
    T__5=6
    T__6=7
    T__7=8
    T__8=9
    T__9=10
    T__10=11
    T__11=12
    T__12=13
    RE=14
    AND=15
    OR=16
    NOT=17
    TRUE=18
    FALSE=19
    IMPL=20
    IFF=21
    EXISTS=22
    FORALL=23
    SUM=24
    COST=25
    LEQ=26
    EQ=27
    GEQ=28
    LT=29
    GT=30
    NEQ=31
    PLUS=32
    MINUS=33
    TIMES=34
    ABS=35
    VARIABLE=36
    ID=37
    INT=38
    WS=39

    def __init__(self, input, output=sys.stdout):
        super(BindPreferenceGrammarParser, self).__init__(input, output=output)
        self.checkVersion("4.7")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None



    class StatementContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(BindPreferenceGrammarParser.StatementContext, self).__init__(parent, invokingState)
            self.parser = parser

        def preference(self):
            return self.getTypedRuleContext(BindPreferenceGrammarParser.PreferenceContext,0)


        def EOF(self):
            return self.getToken(BindPreferenceGrammarParser.EOF, 0)

        def getRuleIndex(self):
            return BindPreferenceGrammarParser.RULE_statement

        def accept(self, visitor):
            if hasattr(visitor, "visitStatement"):
                return visitor.visitStatement(self)
            else:
                return visitor.visitChildren(self)




    def statement(self):

        localctx = BindPreferenceGrammarParser.StatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_statement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 38
            self.preference()
            self.state = 39
            self.match(BindPreferenceGrammarParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class PreferenceContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(BindPreferenceGrammarParser.PreferenceContext, self).__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return BindPreferenceGrammarParser.RULE_preference

     
        def copyFrom(self, ctx):
            super(BindPreferenceGrammarParser.PreferenceContext, self).copyFrom(ctx)



    class ApreferenceExprContext(PreferenceContext):

        def __init__(self, parser, ctx): # actually a BindPreferenceGrammarParser.PreferenceContext)
            super(BindPreferenceGrammarParser.ApreferenceExprContext, self).__init__(parser)
            self.copyFrom(ctx)

        def expr(self):
            return self.getTypedRuleContext(BindPreferenceGrammarParser.ExprContext,0)


        def accept(self, visitor):
            if hasattr(visitor, "visitApreferenceExpr"):
                return visitor.visitApreferenceExpr(self)
            else:
                return visitor.visitChildren(self)


    class ApreferenceLocalContext(PreferenceContext):

        def __init__(self, parser, ctx): # actually a BindPreferenceGrammarParser.PreferenceContext)
            super(BindPreferenceGrammarParser.ApreferenceLocalContext, self).__init__(parser)
            self.copyFrom(ctx)


        def accept(self, visitor):
            if hasattr(visitor, "visitApreferenceLocal"):
                return visitor.visitApreferenceLocal(self)
            else:
                return visitor.visitChildren(self)



    def preference(self):

        localctx = BindPreferenceGrammarParser.PreferenceContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_preference)
        try:
            self.state = 43
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [BindPreferenceGrammarParser.T__0]:
                localctx = BindPreferenceGrammarParser.ApreferenceLocalContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 41
                self.match(BindPreferenceGrammarParser.T__0)
                pass
            elif token in [BindPreferenceGrammarParser.T__5, BindPreferenceGrammarParser.T__6, BindPreferenceGrammarParser.EXISTS, BindPreferenceGrammarParser.FORALL, BindPreferenceGrammarParser.SUM, BindPreferenceGrammarParser.ABS, BindPreferenceGrammarParser.INT]:
                localctx = BindPreferenceGrammarParser.ApreferenceExprContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 42
                self.expr()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class B_exprContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(BindPreferenceGrammarParser.B_exprContext, self).__init__(parent, invokingState)
            self.parser = parser

        def b_term(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(BindPreferenceGrammarParser.B_termContext)
            else:
                return self.getTypedRuleContext(BindPreferenceGrammarParser.B_termContext,i)


        def bool_binary_op(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(BindPreferenceGrammarParser.Bool_binary_opContext)
            else:
                return self.getTypedRuleContext(BindPreferenceGrammarParser.Bool_binary_opContext,i)


        def getRuleIndex(self):
            return BindPreferenceGrammarParser.RULE_b_expr

        def accept(self, visitor):
            if hasattr(visitor, "visitB_expr"):
                return visitor.visitB_expr(self)
            else:
                return visitor.visitChildren(self)




    def b_expr(self):

        localctx = BindPreferenceGrammarParser.B_exprContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_b_expr)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 45
            self.b_term()
            self.state = 51
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,1,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 46
                    self.bool_binary_op()
                    self.state = 47
                    self.b_term() 
                self.state = 53
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,1,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class B_termContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(BindPreferenceGrammarParser.B_termContext, self).__init__(parent, invokingState)
            self.parser = parser

        def b_factor(self):
            return self.getTypedRuleContext(BindPreferenceGrammarParser.B_factorContext,0)


        def unaryOp(self):
            return self.getTypedRuleContext(BindPreferenceGrammarParser.UnaryOpContext,0)


        def getRuleIndex(self):
            return BindPreferenceGrammarParser.RULE_b_term

        def accept(self, visitor):
            if hasattr(visitor, "visitB_term"):
                return visitor.visitB_term(self)
            else:
                return visitor.visitChildren(self)




    def b_term(self):

        localctx = BindPreferenceGrammarParser.B_termContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_b_term)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 55
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==BindPreferenceGrammarParser.NOT:
                self.state = 54
                self.unaryOp()


            self.state = 57
            self.b_factor()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class B_factorContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(BindPreferenceGrammarParser.B_factorContext, self).__init__(parent, invokingState)
            self.parser = parser

        def boolFact(self):
            return self.getTypedRuleContext(BindPreferenceGrammarParser.BoolFactContext,0)


        def relation(self):
            return self.getTypedRuleContext(BindPreferenceGrammarParser.RelationContext,0)


        def getRuleIndex(self):
            return BindPreferenceGrammarParser.RULE_b_factor

        def accept(self, visitor):
            if hasattr(visitor, "visitB_factor"):
                return visitor.visitB_factor(self)
            else:
                return visitor.visitChildren(self)




    def b_factor(self):

        localctx = BindPreferenceGrammarParser.B_factorContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_b_factor)
        try:
            self.state = 61
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [BindPreferenceGrammarParser.TRUE, BindPreferenceGrammarParser.FALSE]:
                self.enterOuterAlt(localctx, 1)
                self.state = 59
                self.boolFact()
                pass
            elif token in [BindPreferenceGrammarParser.T__5, BindPreferenceGrammarParser.T__6, BindPreferenceGrammarParser.EXISTS, BindPreferenceGrammarParser.FORALL, BindPreferenceGrammarParser.SUM, BindPreferenceGrammarParser.ABS, BindPreferenceGrammarParser.INT]:
                self.enterOuterAlt(localctx, 2)
                self.state = 60
                self.relation()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class RelationContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(BindPreferenceGrammarParser.RelationContext, self).__init__(parent, invokingState)
            self.parser = parser

        def expr(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(BindPreferenceGrammarParser.ExprContext)
            else:
                return self.getTypedRuleContext(BindPreferenceGrammarParser.ExprContext,i)


        def comparison_op(self):
            return self.getTypedRuleContext(BindPreferenceGrammarParser.Comparison_opContext,0)


        def getRuleIndex(self):
            return BindPreferenceGrammarParser.RULE_relation

        def accept(self, visitor):
            if hasattr(visitor, "visitRelation"):
                return visitor.visitRelation(self)
            else:
                return visitor.visitChildren(self)




    def relation(self):

        localctx = BindPreferenceGrammarParser.RelationContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_relation)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 63
            self.expr()
            self.state = 67
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,4,self._ctx)
            if la_ == 1:
                self.state = 64
                self.comparison_op()
                self.state = 65
                self.expr()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ExprContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(BindPreferenceGrammarParser.ExprContext, self).__init__(parent, invokingState)
            self.parser = parser

        def term(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(BindPreferenceGrammarParser.TermContext)
            else:
                return self.getTypedRuleContext(BindPreferenceGrammarParser.TermContext,i)


        def arith_binary_op(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(BindPreferenceGrammarParser.Arith_binary_opContext)
            else:
                return self.getTypedRuleContext(BindPreferenceGrammarParser.Arith_binary_opContext,i)


        def getRuleIndex(self):
            return BindPreferenceGrammarParser.RULE_expr

        def accept(self, visitor):
            if hasattr(visitor, "visitExpr"):
                return visitor.visitExpr(self)
            else:
                return visitor.visitChildren(self)




    def expr(self):

        localctx = BindPreferenceGrammarParser.ExprContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_expr)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 69
            self.term()
            self.state = 75
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,5,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 70
                    self.arith_binary_op()
                    self.state = 71
                    self.term() 
                self.state = 77
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,5,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class TermContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(BindPreferenceGrammarParser.TermContext, self).__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return BindPreferenceGrammarParser.RULE_term

     
        def copyFrom(self, ctx):
            super(BindPreferenceGrammarParser.TermContext, self).copyFrom(ctx)



    class AexprQuantifierContext(TermContext):

        def __init__(self, parser, ctx): # actually a BindPreferenceGrammarParser.TermContext)
            super(BindPreferenceGrammarParser.AexprQuantifierContext, self).__init__(parser)
            self.copyFrom(ctx)

        def variable(self):
            return self.getTypedRuleContext(BindPreferenceGrammarParser.VariableContext,0)

        def typeV(self):
            return self.getTypedRuleContext(BindPreferenceGrammarParser.TypeVContext,0)

        def b_expr(self):
            return self.getTypedRuleContext(BindPreferenceGrammarParser.B_exprContext,0)

        def EXISTS(self):
            return self.getToken(BindPreferenceGrammarParser.EXISTS, 0)
        def FORALL(self):
            return self.getToken(BindPreferenceGrammarParser.FORALL, 0)
        def re(self):
            return self.getTypedRuleContext(BindPreferenceGrammarParser.ReContext,0)


        def accept(self, visitor):
            if hasattr(visitor, "visitAexprQuantifier"):
                return visitor.visitAexprQuantifier(self)
            else:
                return visitor.visitChildren(self)


    class AexprSumContext(TermContext):

        def __init__(self, parser, ctx): # actually a BindPreferenceGrammarParser.TermContext)
            super(BindPreferenceGrammarParser.AexprSumContext, self).__init__(parser)
            self.copyFrom(ctx)

        def SUM(self):
            return self.getToken(BindPreferenceGrammarParser.SUM, 0)
        def variable(self):
            return self.getTypedRuleContext(BindPreferenceGrammarParser.VariableContext,0)

        def typeV(self):
            return self.getTypedRuleContext(BindPreferenceGrammarParser.TypeVContext,0)

        def expr(self):
            return self.getTypedRuleContext(BindPreferenceGrammarParser.ExprContext,0)

        def re(self):
            return self.getTypedRuleContext(BindPreferenceGrammarParser.ReContext,0)


        def accept(self, visitor):
            if hasattr(visitor, "visitAexprSum"):
                return visitor.visitAexprSum(self)
            else:
                return visitor.visitChildren(self)


    class AexprBindContext(TermContext):

        def __init__(self, parser, ctx): # actually a BindPreferenceGrammarParser.TermContext)
            super(BindPreferenceGrammarParser.AexprBindContext, self).__init__(parser)
            self.copyFrom(ctx)

        def variable(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(BindPreferenceGrammarParser.VariableContext)
            else:
                return self.getTypedRuleContext(BindPreferenceGrammarParser.VariableContext,i)

        def var_or_port(self):
            return self.getTypedRuleContext(BindPreferenceGrammarParser.Var_or_portContext,0)


        def accept(self, visitor):
            if hasattr(visitor, "visitAexprBind"):
                return visitor.visitAexprBind(self)
            else:
                return visitor.visitChildren(self)


    class AexprBracketsContext(TermContext):

        def __init__(self, parser, ctx): # actually a BindPreferenceGrammarParser.TermContext)
            super(BindPreferenceGrammarParser.AexprBracketsContext, self).__init__(parser)
            self.copyFrom(ctx)

        def b_expr(self):
            return self.getTypedRuleContext(BindPreferenceGrammarParser.B_exprContext,0)


        def accept(self, visitor):
            if hasattr(visitor, "visitAexprBrackets"):
                return visitor.visitAexprBrackets(self)
            else:
                return visitor.visitChildren(self)


    class AexprUnaryArithmeticContext(TermContext):

        def __init__(self, parser, ctx): # actually a BindPreferenceGrammarParser.TermContext)
            super(BindPreferenceGrammarParser.AexprUnaryArithmeticContext, self).__init__(parser)
            self.copyFrom(ctx)

        def arith_unary_op(self):
            return self.getTypedRuleContext(BindPreferenceGrammarParser.Arith_unary_opContext,0)

        def expr(self):
            return self.getTypedRuleContext(BindPreferenceGrammarParser.ExprContext,0)


        def accept(self, visitor):
            if hasattr(visitor, "visitAexprUnaryArithmetic"):
                return visitor.visitAexprUnaryArithmetic(self)
            else:
                return visitor.visitChildren(self)


    class AexprIntContext(TermContext):

        def __init__(self, parser, ctx): # actually a BindPreferenceGrammarParser.TermContext)
            super(BindPreferenceGrammarParser.AexprIntContext, self).__init__(parser)
            self.copyFrom(ctx)

        def INT(self):
            return self.getToken(BindPreferenceGrammarParser.INT, 0)

        def accept(self, visitor):
            if hasattr(visitor, "visitAexprInt"):
                return visitor.visitAexprInt(self)
            else:
                return visitor.visitChildren(self)



    def term(self):

        localctx = BindPreferenceGrammarParser.TermContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_term)
        self._la = 0 # Token type
        try:
            self.state = 119
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [BindPreferenceGrammarParser.EXISTS, BindPreferenceGrammarParser.FORALL]:
                localctx = BindPreferenceGrammarParser.AexprQuantifierContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 78
                _la = self._input.LA(1)
                if not(_la==BindPreferenceGrammarParser.EXISTS or _la==BindPreferenceGrammarParser.FORALL):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 79
                self.variable()
                self.state = 83
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==BindPreferenceGrammarParser.T__1:
                    self.state = 80
                    self.match(BindPreferenceGrammarParser.T__1)
                    self.state = 81
                    self.match(BindPreferenceGrammarParser.T__2)
                    self.state = 82
                    self.re()


                self.state = 85
                self.match(BindPreferenceGrammarParser.T__3)
                self.state = 86
                self.typeV()
                self.state = 87
                self.match(BindPreferenceGrammarParser.T__4)
                self.state = 88
                self.b_expr()
                pass
            elif token in [BindPreferenceGrammarParser.INT]:
                localctx = BindPreferenceGrammarParser.AexprIntContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 90
                self.match(BindPreferenceGrammarParser.INT)
                pass
            elif token in [BindPreferenceGrammarParser.T__5]:
                localctx = BindPreferenceGrammarParser.AexprBindContext(self, localctx)
                self.enterOuterAlt(localctx, 3)
                self.state = 91
                self.match(BindPreferenceGrammarParser.T__5)
                self.state = 92
                self.match(BindPreferenceGrammarParser.T__6)
                self.state = 93
                self.variable()
                self.state = 94
                self.match(BindPreferenceGrammarParser.T__7)
                self.state = 95
                self.variable()
                self.state = 96
                self.match(BindPreferenceGrammarParser.T__7)
                self.state = 97
                self.var_or_port()
                self.state = 98
                self.match(BindPreferenceGrammarParser.T__8)
                pass
            elif token in [BindPreferenceGrammarParser.SUM]:
                localctx = BindPreferenceGrammarParser.AexprSumContext(self, localctx)
                self.enterOuterAlt(localctx, 4)
                self.state = 100
                self.match(BindPreferenceGrammarParser.SUM)
                self.state = 101
                self.variable()
                self.state = 105
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==BindPreferenceGrammarParser.T__1:
                    self.state = 102
                    self.match(BindPreferenceGrammarParser.T__1)
                    self.state = 103
                    self.match(BindPreferenceGrammarParser.T__2)
                    self.state = 104
                    self.re()


                self.state = 107
                self.match(BindPreferenceGrammarParser.T__3)
                self.state = 108
                self.typeV()
                self.state = 109
                self.match(BindPreferenceGrammarParser.T__4)
                self.state = 110
                self.expr()
                pass
            elif token in [BindPreferenceGrammarParser.ABS]:
                localctx = BindPreferenceGrammarParser.AexprUnaryArithmeticContext(self, localctx)
                self.enterOuterAlt(localctx, 5)
                self.state = 112
                self.arith_unary_op()
                self.state = 113
                self.expr()
                pass
            elif token in [BindPreferenceGrammarParser.T__6]:
                localctx = BindPreferenceGrammarParser.AexprBracketsContext(self, localctx)
                self.enterOuterAlt(localctx, 6)
                self.state = 115
                self.match(BindPreferenceGrammarParser.T__6)
                self.state = 116
                self.b_expr()
                self.state = 117
                self.match(BindPreferenceGrammarParser.T__8)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class TypeVContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(BindPreferenceGrammarParser.TypeVContext, self).__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return BindPreferenceGrammarParser.RULE_typeV

     
        def copyFrom(self, ctx):
            super(BindPreferenceGrammarParser.TypeVContext, self).copyFrom(ctx)



    class AtypeVPortsContext(TypeVContext):

        def __init__(self, parser, ctx): # actually a BindPreferenceGrammarParser.TypeVContext)
            super(BindPreferenceGrammarParser.AtypeVPortsContext, self).__init__(parser)
            self.copyFrom(ctx)


        def accept(self, visitor):
            if hasattr(visitor, "visitAtypeVPorts"):
                return visitor.visitAtypeVPorts(self)
            else:
                return visitor.visitChildren(self)


    class AtypeVLocationsContext(TypeVContext):

        def __init__(self, parser, ctx): # actually a BindPreferenceGrammarParser.TypeVContext)
            super(BindPreferenceGrammarParser.AtypeVLocationsContext, self).__init__(parser)
            self.copyFrom(ctx)


        def accept(self, visitor):
            if hasattr(visitor, "visitAtypeVLocations"):
                return visitor.visitAtypeVLocations(self)
            else:
                return visitor.visitChildren(self)


    class AtypeVREContext(TypeVContext):

        def __init__(self, parser, ctx): # actually a BindPreferenceGrammarParser.TypeVContext)
            super(BindPreferenceGrammarParser.AtypeVREContext, self).__init__(parser)
            self.copyFrom(ctx)

        def re(self):
            return self.getTypedRuleContext(BindPreferenceGrammarParser.ReContext,0)


        def accept(self, visitor):
            if hasattr(visitor, "visitAtypeVRE"):
                return visitor.visitAtypeVRE(self)
            else:
                return visitor.visitChildren(self)



    def typeV(self):

        localctx = BindPreferenceGrammarParser.TypeVContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_typeV)
        try:
            self.state = 124
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [BindPreferenceGrammarParser.T__9]:
                localctx = BindPreferenceGrammarParser.AtypeVPortsContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 121
                self.match(BindPreferenceGrammarParser.T__9)
                pass
            elif token in [BindPreferenceGrammarParser.T__10]:
                localctx = BindPreferenceGrammarParser.AtypeVLocationsContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 122
                self.match(BindPreferenceGrammarParser.T__10)
                pass
            elif token in [BindPreferenceGrammarParser.RE]:
                localctx = BindPreferenceGrammarParser.AtypeVREContext(self, localctx)
                self.enterOuterAlt(localctx, 3)
                self.state = 123
                self.re()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class LocsContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(BindPreferenceGrammarParser.LocsContext, self).__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(BindPreferenceGrammarParser.ID, 0)

        def INT(self):
            return self.getToken(BindPreferenceGrammarParser.INT, 0)

        def getRuleIndex(self):
            return BindPreferenceGrammarParser.RULE_locs

        def accept(self, visitor):
            if hasattr(visitor, "visitLocs"):
                return visitor.visitLocs(self)
            else:
                return visitor.visitChildren(self)




    def locs(self):

        localctx = BindPreferenceGrammarParser.LocsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_locs)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 126
            self.match(BindPreferenceGrammarParser.ID)
            self.state = 130
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==BindPreferenceGrammarParser.T__11:
                self.state = 127
                self.match(BindPreferenceGrammarParser.T__11)
                self.state = 128
                self.match(BindPreferenceGrammarParser.INT)
                self.state = 129
                self.match(BindPreferenceGrammarParser.T__12)


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Var_or_portContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(BindPreferenceGrammarParser.Var_or_portContext, self).__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return BindPreferenceGrammarParser.RULE_var_or_port

     
        def copyFrom(self, ctx):
            super(BindPreferenceGrammarParser.Var_or_portContext, self).copyFrom(ctx)



    class AvarContext(Var_or_portContext):

        def __init__(self, parser, ctx): # actually a BindPreferenceGrammarParser.Var_or_portContext)
            super(BindPreferenceGrammarParser.AvarContext, self).__init__(parser)
            self.copyFrom(ctx)

        def variable(self):
            return self.getTypedRuleContext(BindPreferenceGrammarParser.VariableContext,0)


        def accept(self, visitor):
            if hasattr(visitor, "visitAvar"):
                return visitor.visitAvar(self)
            else:
                return visitor.visitChildren(self)


    class AportContext(Var_or_portContext):

        def __init__(self, parser, ctx): # actually a BindPreferenceGrammarParser.Var_or_portContext)
            super(BindPreferenceGrammarParser.AportContext, self).__init__(parser)
            self.copyFrom(ctx)

        def ID(self):
            return self.getToken(BindPreferenceGrammarParser.ID, 0)

        def accept(self, visitor):
            if hasattr(visitor, "visitAport"):
                return visitor.visitAport(self)
            else:
                return visitor.visitChildren(self)



    def var_or_port(self):

        localctx = BindPreferenceGrammarParser.Var_or_portContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_var_or_port)
        try:
            self.state = 134
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [BindPreferenceGrammarParser.ID]:
                localctx = BindPreferenceGrammarParser.AportContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 132
                self.match(BindPreferenceGrammarParser.ID)
                pass
            elif token in [BindPreferenceGrammarParser.VARIABLE]:
                localctx = BindPreferenceGrammarParser.AvarContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 133
                self.variable()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Bool_binary_opContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(BindPreferenceGrammarParser.Bool_binary_opContext, self).__init__(parent, invokingState)
            self.parser = parser

        def AND(self):
            return self.getToken(BindPreferenceGrammarParser.AND, 0)

        def OR(self):
            return self.getToken(BindPreferenceGrammarParser.OR, 0)

        def IMPL(self):
            return self.getToken(BindPreferenceGrammarParser.IMPL, 0)

        def IFF(self):
            return self.getToken(BindPreferenceGrammarParser.IFF, 0)

        def getRuleIndex(self):
            return BindPreferenceGrammarParser.RULE_bool_binary_op

        def accept(self, visitor):
            if hasattr(visitor, "visitBool_binary_op"):
                return visitor.visitBool_binary_op(self)
            else:
                return visitor.visitChildren(self)




    def bool_binary_op(self):

        localctx = BindPreferenceGrammarParser.Bool_binary_opContext(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_bool_binary_op)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 136
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << BindPreferenceGrammarParser.AND) | (1 << BindPreferenceGrammarParser.OR) | (1 << BindPreferenceGrammarParser.IMPL) | (1 << BindPreferenceGrammarParser.IFF))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Arith_binary_opContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(BindPreferenceGrammarParser.Arith_binary_opContext, self).__init__(parent, invokingState)
            self.parser = parser

        def PLUS(self):
            return self.getToken(BindPreferenceGrammarParser.PLUS, 0)

        def MINUS(self):
            return self.getToken(BindPreferenceGrammarParser.MINUS, 0)

        def TIMES(self):
            return self.getToken(BindPreferenceGrammarParser.TIMES, 0)

        def getRuleIndex(self):
            return BindPreferenceGrammarParser.RULE_arith_binary_op

        def accept(self, visitor):
            if hasattr(visitor, "visitArith_binary_op"):
                return visitor.visitArith_binary_op(self)
            else:
                return visitor.visitChildren(self)




    def arith_binary_op(self):

        localctx = BindPreferenceGrammarParser.Arith_binary_opContext(self, self._ctx, self.state)
        self.enterRule(localctx, 24, self.RULE_arith_binary_op)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 138
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << BindPreferenceGrammarParser.PLUS) | (1 << BindPreferenceGrammarParser.MINUS) | (1 << BindPreferenceGrammarParser.TIMES))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Arith_unary_opContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(BindPreferenceGrammarParser.Arith_unary_opContext, self).__init__(parent, invokingState)
            self.parser = parser

        def ABS(self):
            return self.getToken(BindPreferenceGrammarParser.ABS, 0)

        def getRuleIndex(self):
            return BindPreferenceGrammarParser.RULE_arith_unary_op

        def accept(self, visitor):
            if hasattr(visitor, "visitArith_unary_op"):
                return visitor.visitArith_unary_op(self)
            else:
                return visitor.visitChildren(self)




    def arith_unary_op(self):

        localctx = BindPreferenceGrammarParser.Arith_unary_opContext(self, self._ctx, self.state)
        self.enterRule(localctx, 26, self.RULE_arith_unary_op)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 140
            self.match(BindPreferenceGrammarParser.ABS)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Comparison_opContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(BindPreferenceGrammarParser.Comparison_opContext, self).__init__(parent, invokingState)
            self.parser = parser

        def LEQ(self):
            return self.getToken(BindPreferenceGrammarParser.LEQ, 0)

        def EQ(self):
            return self.getToken(BindPreferenceGrammarParser.EQ, 0)

        def GEQ(self):
            return self.getToken(BindPreferenceGrammarParser.GEQ, 0)

        def LT(self):
            return self.getToken(BindPreferenceGrammarParser.LT, 0)

        def GT(self):
            return self.getToken(BindPreferenceGrammarParser.GT, 0)

        def NEQ(self):
            return self.getToken(BindPreferenceGrammarParser.NEQ, 0)

        def getRuleIndex(self):
            return BindPreferenceGrammarParser.RULE_comparison_op

        def accept(self, visitor):
            if hasattr(visitor, "visitComparison_op"):
                return visitor.visitComparison_op(self)
            else:
                return visitor.visitChildren(self)




    def comparison_op(self):

        localctx = BindPreferenceGrammarParser.Comparison_opContext(self, self._ctx, self.state)
        self.enterRule(localctx, 28, self.RULE_comparison_op)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 142
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << BindPreferenceGrammarParser.LEQ) | (1 << BindPreferenceGrammarParser.EQ) | (1 << BindPreferenceGrammarParser.GEQ) | (1 << BindPreferenceGrammarParser.LT) | (1 << BindPreferenceGrammarParser.GT) | (1 << BindPreferenceGrammarParser.NEQ))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class UnaryOpContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(BindPreferenceGrammarParser.UnaryOpContext, self).__init__(parent, invokingState)
            self.parser = parser

        def NOT(self):
            return self.getToken(BindPreferenceGrammarParser.NOT, 0)

        def getRuleIndex(self):
            return BindPreferenceGrammarParser.RULE_unaryOp

        def accept(self, visitor):
            if hasattr(visitor, "visitUnaryOp"):
                return visitor.visitUnaryOp(self)
            else:
                return visitor.visitChildren(self)




    def unaryOp(self):

        localctx = BindPreferenceGrammarParser.UnaryOpContext(self, self._ctx, self.state)
        self.enterRule(localctx, 30, self.RULE_unaryOp)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 144
            self.match(BindPreferenceGrammarParser.NOT)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class BoolFactContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(BindPreferenceGrammarParser.BoolFactContext, self).__init__(parent, invokingState)
            self.parser = parser

        def TRUE(self):
            return self.getToken(BindPreferenceGrammarParser.TRUE, 0)

        def FALSE(self):
            return self.getToken(BindPreferenceGrammarParser.FALSE, 0)

        def getRuleIndex(self):
            return BindPreferenceGrammarParser.RULE_boolFact

        def accept(self, visitor):
            if hasattr(visitor, "visitBoolFact"):
                return visitor.visitBoolFact(self)
            else:
                return visitor.visitChildren(self)




    def boolFact(self):

        localctx = BindPreferenceGrammarParser.BoolFactContext(self, self._ctx, self.state)
        self.enterRule(localctx, 32, self.RULE_boolFact)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 146
            _la = self._input.LA(1)
            if not(_la==BindPreferenceGrammarParser.TRUE or _la==BindPreferenceGrammarParser.FALSE):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class VariableContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(BindPreferenceGrammarParser.VariableContext, self).__init__(parent, invokingState)
            self.parser = parser

        def VARIABLE(self):
            return self.getToken(BindPreferenceGrammarParser.VARIABLE, 0)

        def getRuleIndex(self):
            return BindPreferenceGrammarParser.RULE_variable

        def accept(self, visitor):
            if hasattr(visitor, "visitVariable"):
                return visitor.visitVariable(self)
            else:
                return visitor.visitChildren(self)




    def variable(self):

        localctx = BindPreferenceGrammarParser.VariableContext(self, self._ctx, self.state)
        self.enterRule(localctx, 34, self.RULE_variable)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 148
            self.match(BindPreferenceGrammarParser.VARIABLE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ReContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(BindPreferenceGrammarParser.ReContext, self).__init__(parent, invokingState)
            self.parser = parser

        def RE(self):
            return self.getToken(BindPreferenceGrammarParser.RE, 0)

        def getRuleIndex(self):
            return BindPreferenceGrammarParser.RULE_re

        def accept(self, visitor):
            if hasattr(visitor, "visitRe"):
                return visitor.visitRe(self)
            else:
                return visitor.visitChildren(self)




    def re(self):

        localctx = BindPreferenceGrammarParser.ReContext(self, self._ctx, self.state)
        self.enterRule(localctx, 36, self.RULE_re)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 150
            self.match(BindPreferenceGrammarParser.RE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





