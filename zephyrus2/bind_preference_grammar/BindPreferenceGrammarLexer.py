# Generated from BindPreferenceGrammar.g4 by ANTLR 4.7
# encoding: utf-8
from __future__ import print_function
from antlr4 import *
from io import StringIO
import sys


def serializedATN():
    with StringIO() as buf:
        buf.write(u"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2")
        buf.write(u")\u00f7\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4")
        buf.write(u"\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r")
        buf.write(u"\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22")
        buf.write(u"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4")
        buf.write(u"\30\t\30\4\31\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35")
        buf.write(u"\t\35\4\36\t\36\4\37\t\37\4 \t \4!\t!\4\"\t\"\4#\t#\4")
        buf.write(u"$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\3\2\3\2\3\2\3\2\3\2\3")
        buf.write(u"\2\3\3\3\3\3\3\3\4\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\6\3")
        buf.write(u"\6\3\7\3\7\3\7\3\7\3\7\3\b\3\b\3\t\3\t\3\n\3\n\3\13\3")
        buf.write(u"\13\3\13\3\13\3\13\3\13\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3")
        buf.write(u"\f\3\f\3\f\3\r\3\r\3\16\3\16\3\17\3\17\3\17\3\17\6\17")
        buf.write(u"\u0088\n\17\r\17\16\17\u0089\3\17\3\17\3\20\3\20\3\20")
        buf.write(u"\3\20\3\21\3\21\3\21\3\22\3\22\3\22\3\22\3\23\3\23\3")
        buf.write(u"\23\3\23\3\23\3\24\3\24\3\24\3\24\3\24\3\24\3\25\3\25")
        buf.write(u"\3\25\3\25\3\25\3\26\3\26\3\26\3\26\3\27\3\27\3\27\3")
        buf.write(u"\27\3\27\3\27\3\27\3\30\3\30\3\30\3\30\3\30\3\30\3\30")
        buf.write(u"\3\31\3\31\3\31\3\31\3\32\3\32\3\32\3\32\3\32\3\33\3")
        buf.write(u"\33\3\33\3\34\3\34\3\35\3\35\3\35\3\36\3\36\3\37\3\37")
        buf.write(u"\3 \3 \3 \3!\3!\3\"\3\"\3#\3#\3$\3$\3$\3$\3%\3%\3%\7")
        buf.write(u"%\u00e0\n%\f%\16%\u00e3\13%\3&\3&\7&\u00e7\n&\f&\16&")
        buf.write(u"\u00ea\13&\3\'\6\'\u00ed\n\'\r\'\16\'\u00ee\3(\6(\u00f2")
        buf.write(u"\n(\r(\16(\u00f3\3(\3(\2\2)\3\3\5\4\7\5\t\6\13\7\r\b")
        buf.write(u"\17\t\21\n\23\13\25\f\27\r\31\16\33\17\35\20\37\21!\22")
        buf.write(u"#\23%\24\'\25)\26+\27-\30/\31\61\32\63\33\65\34\67\35")
        buf.write(u"9\36;\37= ?!A\"C#E$G%I&K\'M(O)\3\2\7\n\2&&**,-/\60\62")
        buf.write(u";AACac\177\5\2C\\aac|\6\2\62;C\\aac|\3\2\62;\5\2\13\f")
        buf.write(u"\17\17\"\"\2\u00fc\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2")
        buf.write(u"\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2")
        buf.write(u"\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2")
        buf.write(u"\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2")
        buf.write(u"\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2")
        buf.write(u"\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3\2\2\2\2\61\3\2\2\2\2")
        buf.write(u"\63\3\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2\29\3\2\2\2\2;\3")
        buf.write(u"\2\2\2\2=\3\2\2\2\2?\3\2\2\2\2A\3\2\2\2\2C\3\2\2\2\2")
        buf.write(u"E\3\2\2\2\2G\3\2\2\2\2I\3\2\2\2\2K\3\2\2\2\2M\3\2\2\2")
        buf.write(u"\2O\3\2\2\2\3Q\3\2\2\2\5W\3\2\2\2\7Z\3\2\2\2\t_\3\2\2")
        buf.write(u"\2\13b\3\2\2\2\rd\3\2\2\2\17i\3\2\2\2\21k\3\2\2\2\23")
        buf.write(u"m\3\2\2\2\25o\3\2\2\2\27u\3\2\2\2\31\177\3\2\2\2\33\u0081")
        buf.write(u"\3\2\2\2\35\u0083\3\2\2\2\37\u008d\3\2\2\2!\u0091\3\2")
        buf.write(u"\2\2#\u0094\3\2\2\2%\u0098\3\2\2\2\'\u009d\3\2\2\2)\u00a3")
        buf.write(u"\3\2\2\2+\u00a8\3\2\2\2-\u00ac\3\2\2\2/\u00b3\3\2\2\2")
        buf.write(u"\61\u00ba\3\2\2\2\63\u00be\3\2\2\2\65\u00c3\3\2\2\2\67")
        buf.write(u"\u00c6\3\2\2\29\u00c8\3\2\2\2;\u00cb\3\2\2\2=\u00cd\3")
        buf.write(u"\2\2\2?\u00cf\3\2\2\2A\u00d2\3\2\2\2C\u00d4\3\2\2\2E")
        buf.write(u"\u00d6\3\2\2\2G\u00d8\3\2\2\2I\u00dc\3\2\2\2K\u00e4\3")
        buf.write(u"\2\2\2M\u00ec\3\2\2\2O\u00f1\3\2\2\2QR\7n\2\2RS\7q\2")
        buf.write(u"\2ST\7e\2\2TU\7c\2\2UV\7n\2\2V\4\3\2\2\2WX\7q\2\2XY\7")
        buf.write(u"h\2\2Y\6\3\2\2\2Z[\7v\2\2[\\\7{\2\2\\]\7r\2\2]^\7g\2")
        buf.write(u"\2^\b\3\2\2\2_`\7k\2\2`a\7p\2\2a\n\3\2\2\2bc\7<\2\2c")
        buf.write(u"\f\3\2\2\2de\7d\2\2ef\7k\2\2fg\7p\2\2gh\7f\2\2h\16\3")
        buf.write(u"\2\2\2ij\7*\2\2j\20\3\2\2\2kl\7.\2\2l\22\3\2\2\2mn\7")
        buf.write(u"+\2\2n\24\3\2\2\2op\7r\2\2pq\7q\2\2qr\7t\2\2rs\7v\2\2")
        buf.write(u"st\7u\2\2t\26\3\2\2\2uv\7n\2\2vw\7q\2\2wx\7e\2\2xy\7")
        buf.write(u"c\2\2yz\7v\2\2z{\7k\2\2{|\7q\2\2|}\7p\2\2}~\7u\2\2~\30")
        buf.write(u"\3\2\2\2\177\u0080\7]\2\2\u0080\32\3\2\2\2\u0081\u0082")
        buf.write(u"\7_\2\2\u0082\34\3\2\2\2\u0083\u0087\7)\2\2\u0084\u0088")
        buf.write(u"\t\2\2\2\u0085\u0086\7+\2\2\u0086\u0088\7\"\2\2\u0087")
        buf.write(u"\u0084\3\2\2\2\u0087\u0085\3\2\2\2\u0088\u0089\3\2\2")
        buf.write(u"\2\u0089\u0087\3\2\2\2\u0089\u008a\3\2\2\2\u008a\u008b")
        buf.write(u"\3\2\2\2\u008b\u008c\7)\2\2\u008c\36\3\2\2\2\u008d\u008e")
        buf.write(u"\7c\2\2\u008e\u008f\7p\2\2\u008f\u0090\7f\2\2\u0090 ")
        buf.write(u"\3\2\2\2\u0091\u0092\7q\2\2\u0092\u0093\7t\2\2\u0093")
        buf.write(u"\"\3\2\2\2\u0094\u0095\7p\2\2\u0095\u0096\7q\2\2\u0096")
        buf.write(u"\u0097\7v\2\2\u0097$\3\2\2\2\u0098\u0099\7v\2\2\u0099")
        buf.write(u"\u009a\7t\2\2\u009a\u009b\7w\2\2\u009b\u009c\7g\2\2\u009c")
        buf.write(u"&\3\2\2\2\u009d\u009e\7h\2\2\u009e\u009f\7c\2\2\u009f")
        buf.write(u"\u00a0\7n\2\2\u00a0\u00a1\7u\2\2\u00a1\u00a2\7g\2\2\u00a2")
        buf.write(u"(\3\2\2\2\u00a3\u00a4\7k\2\2\u00a4\u00a5\7o\2\2\u00a5")
        buf.write(u"\u00a6\7r\2\2\u00a6\u00a7\7n\2\2\u00a7*\3\2\2\2\u00a8")
        buf.write(u"\u00a9\7k\2\2\u00a9\u00aa\7h\2\2\u00aa\u00ab\7h\2\2\u00ab")
        buf.write(u",\3\2\2\2\u00ac\u00ad\7g\2\2\u00ad\u00ae\7z\2\2\u00ae")
        buf.write(u"\u00af\7k\2\2\u00af\u00b0\7u\2\2\u00b0\u00b1\7v\2\2\u00b1")
        buf.write(u"\u00b2\7u\2\2\u00b2.\3\2\2\2\u00b3\u00b4\7h\2\2\u00b4")
        buf.write(u"\u00b5\7q\2\2\u00b5\u00b6\7t\2\2\u00b6\u00b7\7c\2\2\u00b7")
        buf.write(u"\u00b8\7n\2\2\u00b8\u00b9\7n\2\2\u00b9\60\3\2\2\2\u00ba")
        buf.write(u"\u00bb\7u\2\2\u00bb\u00bc\7w\2\2\u00bc\u00bd\7o\2\2\u00bd")
        buf.write(u"\62\3\2\2\2\u00be\u00bf\7e\2\2\u00bf\u00c0\7q\2\2\u00c0")
        buf.write(u"\u00c1\7u\2\2\u00c1\u00c2\7v\2\2\u00c2\64\3\2\2\2\u00c3")
        buf.write(u"\u00c4\7>\2\2\u00c4\u00c5\7?\2\2\u00c5\66\3\2\2\2\u00c6")
        buf.write(u"\u00c7\7?\2\2\u00c78\3\2\2\2\u00c8\u00c9\7@\2\2\u00c9")
        buf.write(u"\u00ca\7?\2\2\u00ca:\3\2\2\2\u00cb\u00cc\7>\2\2\u00cc")
        buf.write(u"<\3\2\2\2\u00cd\u00ce\7@\2\2\u00ce>\3\2\2\2\u00cf\u00d0")
        buf.write(u"\7#\2\2\u00d0\u00d1\7?\2\2\u00d1@\3\2\2\2\u00d2\u00d3")
        buf.write(u"\7-\2\2\u00d3B\3\2\2\2\u00d4\u00d5\7/\2\2\u00d5D\3\2")
        buf.write(u"\2\2\u00d6\u00d7\7,\2\2\u00d7F\3\2\2\2\u00d8\u00d9\7")
        buf.write(u"c\2\2\u00d9\u00da\7d\2\2\u00da\u00db\7u\2\2\u00dbH\3")
        buf.write(u"\2\2\2\u00dc\u00dd\7A\2\2\u00dd\u00e1\t\3\2\2\u00de\u00e0")
        buf.write(u"\t\4\2\2\u00df\u00de\3\2\2\2\u00e0\u00e3\3\2\2\2\u00e1")
        buf.write(u"\u00df\3\2\2\2\u00e1\u00e2\3\2\2\2\u00e2J\3\2\2\2\u00e3")
        buf.write(u"\u00e1\3\2\2\2\u00e4\u00e8\t\3\2\2\u00e5\u00e7\t\4\2")
        buf.write(u"\2\u00e6\u00e5\3\2\2\2\u00e7\u00ea\3\2\2\2\u00e8\u00e6")
        buf.write(u"\3\2\2\2\u00e8\u00e9\3\2\2\2\u00e9L\3\2\2\2\u00ea\u00e8")
        buf.write(u"\3\2\2\2\u00eb\u00ed\t\5\2\2\u00ec\u00eb\3\2\2\2\u00ed")
        buf.write(u"\u00ee\3\2\2\2\u00ee\u00ec\3\2\2\2\u00ee\u00ef\3\2\2")
        buf.write(u"\2\u00efN\3\2\2\2\u00f0\u00f2\t\6\2\2\u00f1\u00f0\3\2")
        buf.write(u"\2\2\u00f2\u00f3\3\2\2\2\u00f3\u00f1\3\2\2\2\u00f3\u00f4")
        buf.write(u"\3\2\2\2\u00f4\u00f5\3\2\2\2\u00f5\u00f6\b(\2\2\u00f6")
        buf.write(u"P\3\2\2\2\t\2\u0087\u0089\u00e1\u00e8\u00ee\u00f3\3\b")
        buf.write(u"\2\2")
        return buf.getvalue()


class BindPreferenceGrammarLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    T__0 = 1
    T__1 = 2
    T__2 = 3
    T__3 = 4
    T__4 = 5
    T__5 = 6
    T__6 = 7
    T__7 = 8
    T__8 = 9
    T__9 = 10
    T__10 = 11
    T__11 = 12
    T__12 = 13
    RE = 14
    AND = 15
    OR = 16
    NOT = 17
    TRUE = 18
    FALSE = 19
    IMPL = 20
    IFF = 21
    EXISTS = 22
    FORALL = 23
    SUM = 24
    COST = 25
    LEQ = 26
    EQ = 27
    GEQ = 28
    LT = 29
    GT = 30
    NEQ = 31
    PLUS = 32
    MINUS = 33
    TIMES = 34
    ABS = 35
    VARIABLE = 36
    ID = 37
    INT = 38
    WS = 39

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ u"DEFAULT_MODE" ]

    literalNames = [ u"<INVALID>",
            u"'local'", u"'of'", u"'type'", u"'in'", u"':'", u"'bind'", 
            u"'('", u"','", u"')'", u"'ports'", u"'locations'", u"'['", 
            u"']'", u"'and'", u"'or'", u"'not'", u"'true'", u"'false'", 
            u"'impl'", u"'iff'", u"'exists'", u"'forall'", u"'sum'", u"'cost'", 
            u"'<='", u"'='", u"'>='", u"'<'", u"'>'", u"'!='", u"'+'", u"'-'", 
            u"'*'", u"'abs'" ]

    symbolicNames = [ u"<INVALID>",
            u"RE", u"AND", u"OR", u"NOT", u"TRUE", u"FALSE", u"IMPL", u"IFF", 
            u"EXISTS", u"FORALL", u"SUM", u"COST", u"LEQ", u"EQ", u"GEQ", 
            u"LT", u"GT", u"NEQ", u"PLUS", u"MINUS", u"TIMES", u"ABS", u"VARIABLE", 
            u"ID", u"INT", u"WS" ]

    ruleNames = [ u"T__0", u"T__1", u"T__2", u"T__3", u"T__4", u"T__5", 
                  u"T__6", u"T__7", u"T__8", u"T__9", u"T__10", u"T__11", 
                  u"T__12", u"RE", u"AND", u"OR", u"NOT", u"TRUE", u"FALSE", 
                  u"IMPL", u"IFF", u"EXISTS", u"FORALL", u"SUM", u"COST", 
                  u"LEQ", u"EQ", u"GEQ", u"LT", u"GT", u"NEQ", u"PLUS", 
                  u"MINUS", u"TIMES", u"ABS", u"VARIABLE", u"ID", u"INT", 
                  u"WS" ]

    grammarFileName = u"BindPreferenceGrammar.g4"

    def __init__(self, input=None, output=sys.stdout):
        super(BindPreferenceGrammarLexer, self).__init__(input, output=output)
        self.checkVersion("4.7")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


