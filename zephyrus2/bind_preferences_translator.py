""" Module for translating the specification into binding preferences"""
__author__ = "Jacopo Mauro"
__copyright__ = "Copyright 2016, Jacopo Mauro"
__license__ = "ISC"
__version__ = "0.1"
__maintainer__ = "Jacopo Mauro"
__email__ = "mauro.jacopo@gmail.com"
__status__ = "Prototype"

from antlr4 import *
from antlr4 import InputStream
from bind_preference_grammar.BindPreferenceGrammarLexer import BindPreferenceGrammarLexer
from bind_preference_grammar.BindPreferenceGrammarParser import BindPreferenceGrammarParser
from bind_preference_grammar.BindPreferenceGrammarVisitor import BindPreferenceGrammarVisitor

import re


# import settings

class SpecificationParsingException(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


class MyVisitor(BindPreferenceGrammarVisitor):
    def __init__(self, inst_to_comp, inst_to_loc,
                 comp_to_int, port_to_int,
                 loc_to_int):
        """
    init some variables
    """
        BindPreferenceGrammarVisitor.__init__(self)
        self.inst_to_comp = inst_to_comp
        self.inst_to_loc = inst_to_loc
        self.comp_to_int = comp_to_int
        self.port_to_int = port_to_int
        self.loc_to_int = loc_to_int

    def getInstanceSet(self, ty, locations):
        """ return the string with the range of values for a sum, exists and forall statement
        """
        locations = locations.strip()
        if locations == "ports":
            return "1.." + str(len(self.port_to_int))
        elif locations == "locations":  # ty is all the locations
            s = ""
            for i in self.inst_to_comp.keys():
                if re.match(ty, self.inst_to_comp[i]):
                    if not s:
                        s = " { " + str(i) + " }"
                    else:
                        s += " union { " + str(i) + " }"
            return s
        else: # regular expression
            s = ""
            for i in self.inst_to_comp.keys():
                if re.match(ty, self.inst_to_comp[i]) and re.match(locations,self.inst_to_loc[i][0]):
                    if not s:
                        s = " { " + str(i) + " }"
                    else:
                        s += " union { " + str(i) + " }"
            return s



        #   def getInstanceSet(self,name):
        #     "given a name finds the set of location of the component types"
        #     s = []
        #     if (name,0) in self.loc_to_int:
        #         counter = 1
        #         while (name,counter) in self.loc_to_int:
        #           counter += 1
        #         s += str(self.loc_to_int[(name,0)]) + ".." + \
        #           str(self.loc_to_int[(name,counter-1)])
        #     else:
        #       raise SpecificationParsingException("\"" + name +
        #         "\" is not a valid location name")
        #     return s

        #   def getDCsets(self,pattern):
        #     matches = set([])
        #     for (x,_) in self.loc_to_int:
        #       if re.match(pattern,x):
        #         matches.add(x)
        #     ls = list(matches)
        #     if not ls:
        #       matches = set([])
        #       for (x,_) in self.loc_to_int:
        #         matches.add(x)
        #       raise SpecificationParsingException("\"" + pattern +
        #         "\" does not match with any DC: " + str(matches))
        #     else:
        #       s = self.getDCset(ls[0])
        #       for i in ls[1:]:
        #         s += ' union ' + self.getDCset(i)
        #       return s

    def defaultResult(self):
        return ""

    def visitTerminal(self, node):

        switcher = {
            'or': "\\/",
            'and': '/\\',
            'impl': "->",
            'iff': "<->"
        }
        txt = node.getText()
        if txt in switcher:
            return switcher[txt]
        else:
            return txt

    def aggregateResult(self, aggregate, nextResult):
        return aggregate + " " + nextResult

    def visitErrorNode(self, node):
        token = node.getSymbol()
        raise SpecificationParsingException("Erroneous Node at line " +
                                            str(token.line) + ", column " + str(token.column) + ": '" +
                                            str(token.text) + "'")

    def visitApreferenceLocal(self, ctx):
        return \
            """
      sum ( i1 in insts, i2 in insts, p in ports )(
        if locations[i1] = locations[i2]
        then binding_matrix[i1,i2,p]
        else 0
        endif )    
      """

    def visitStatement(self, ctx):
        return ctx.getChild(0).accept(self)

    def visitVariable(self, ctx):
        return ctx.getChild(0).accept(self)[1:]

    def visitRe(self, ctx):
        return ctx.getChild(0).accept(self)[1:-1]

    # #   def visitAformulaBrackets(self, ctx):
    # #     return "( " +  ctx.getChild(1).accept(self) + " )"
    #
    def visitAexprQuantifier(self, ctx):
        quantifier = ctx.getChild(0).accept(self)
        var = ctx.getChild(1).accept(self)
        ty = ".*"
        if ctx.getChild(2).accept(self).strip() == "of":
            ty = ctx.getChild(4).accept(self).strip()
        locs = ctx.getChild(ctx.getChildCount() - 3).accept(self)
        expr = ctx.getChild(ctx.getChildCount() - 1).accept(self)
        inst = self.getInstanceSet(ty, locs)

        if not inst:  # no instance matched
            if quantifier.strip() == "exists":
                return "false"
            else:
                return "true"
        return quantifier + " (" + var + " in " + inst + ")( " + expr + " )"

    def visitAexprSum(self, ctx):
        var = ctx.getChild(1).accept(self)
        ty = ".*"
        if ctx.getChild(2).accept(self).strip() == "of":
            ty = ctx.getChild(4).accept(self).strip()
        locs = ctx.getChild(ctx.getChildCount() - 3).accept(self)
        expr = ctx.getChild(ctx.getChildCount() - 1).accept(self)
        inst = self.getInstanceSet(ty, locs)
        if not inst:  # no instance matched
            return "0"
        return "sum (" + var + " in " + inst + ")( " + expr + " )"

    def visitAexprBind(self, ctx):
        s = "binding_matrix[" + ctx.getChild(2).accept(self) + ","
        s += ctx.getChild(4).accept(self) + ","
        s += ctx.getChild(6).accept(self) + "]"
        return s

    def visitAport(self, ctx):
        port = ctx.getChild(0).accept(self)
        if port not in self.port_to_int:
            raise SpecificationParsingException("The string " + port +
                                                " is not a valid port name")
        else:
            return str(self.port_to_int[port])


# #   def visitAexprBrackets(self, ctx):
# #     return "( " +  ctx.getChild(1).accept(self) + " )"
#     
#   def visitAexprId(self, ctx):
#     comp = ctx.getChild(0).accept(self)
#     if comp in self.comp_to_int:
#       return "sum ( l in locations) ( comp_locations[l," + \
#         str(self.comp_to_int[comp]) + "])"
#     else:
#       raise SpecificationParsingException("\"" + comp +
#         "\" is not a valid component name")
#     return ""
#     
#   def visitAexprIDLoc(self, ctx):
#     loc = ctx.getChild(0).accept(self)
#     num = int(ctx.getChild(2).accept(self))
#     obj = ctx.getChild(5).accept(self)
#     if (loc,num) in self.loc_to_int:
#       if obj in self.comp_to_int:
#         self.constraints_comp_in_loc = True
#         self.constraints_over_loc = True
#         return "comp_locations[" + str(self.loc_to_int[(loc,num)]) + "," + \
#           str(self.comp_to_int[obj]) + "]"
#       elif obj in self.res_to_int:
#         # symmetry constraint ok because they trigger only if loc has same resources
#         return "resource_provisions[ " + str(self.loc_to_int[(loc,num)]) + "," + \
#           str(self.res_to_int[obj]) + "]"
#       else:
#         raise SpecificationParsingException("\"" + obj +
#           "\" is not a valid component or resource name")
#     else:
#       raise SpecificationParsingException(loc + "[" + str(num) +
#          "] is not a valid location")
#     return "" 
#   
#   def visitAexprVariable(self, ctx):
#     return "sum ( l in locations) ( comp_locations[l," + \
#         ctx.getChild(0).accept(self) + "])"
# 
# 
#    
#   def visitAexprVariableId(self, ctx):
#     var = ctx.getChild(0).accept(self)
#     obj = ctx.getChild(2).accept(self)
#     if obj in self.comp_to_int:
#       self.constraints_over_loc = True
#       return "comp_locations[" + var + "," + \
#         str(self.comp_to_int[obj]) + "]"
#     elif obj in self.res_to_int:
#       # symmetry constraint ok because they trigger only if loc has same resources
#       return "resource_provisions[ " + var + "," + \
#         str(self.res_to_int[obj]) + "]"
#     else:
#       raise SpecificationParsingException("\"" + obj +
#         "\" is not a valid component or resource name")
#     return ""
# 
# 
#   def visitAexprLocVariable(self, ctx):
#     loc = ctx.getChild(0).accept(self)
#     num = int(ctx.getChild(2).accept(self))
#     var = ctx.getChild(5).accept(self)
#     if (loc,num) in self.loc_to_int:
#       self.constraints_comp_in_loc = True
#       self.constraints_over_loc = True
#       return "comp_locations[" + str(self.loc_to_int[(loc,num)]) + "," + \
#           var + "]"
#     else:
#       raise SpecificationParsingException(loc + "[" + str(num) +
#          "] is not a valid location")
#     return "" 
# 
# 
#   def visitAexprVariablevariable(self, ctx):
#     self.constraints_over_loc = True
#     return "comp_locations[" + ctx.getChild(0).accept(self) + "," + \
#           ctx.getChild(2).accept(self) + "]"
# 
# 


def translate_preference(s,
                         inst_to_comp, inst_to_loc,
                         port_to_int,
                         comp_to_int,
                         loc_to_int):
    lexer = BindPreferenceGrammarLexer(InputStream(s))
    stream = CommonTokenStream(lexer)
    parser = BindPreferenceGrammarParser(stream)
    tree = parser.statement()
    visitor = MyVisitor(inst_to_comp, inst_to_loc,
                        comp_to_int, port_to_int,
                        loc_to_int)
    return visitor.visit(tree)


def translate_preferences(
        preferences,
        inst_to_comp, inst_to_loc,
        port_to_int,
        comp_to_int,
        loc_to_int):
    s = "array[1.." + str(1 + len(preferences)) + "]" \
                                                  " of var int: obj_array;\n"
    counter = 1
    for i in preferences:
        s += "constraint obj_array[" + str(counter) + "] = -"
        s += translate_preference(
            i,
            inst_to_comp, inst_to_loc,
            port_to_int,
            comp_to_int,
            loc_to_int)
        s += ";\n"
        counter += 1

    # minimize number of bindings
    s += "constraint obj_array[" + str(counter) + "] = "
    s += "sum ( i1 in insts, i2 in insts, p in ports )( binding_matrix[i1,i2,p] );\n"
    return (s)
