#!/usr/bin/python

import json
import re
from subprocess import Popen,PIPE
import uuid
import math
import logging as log
import sys
# log.basicConfig(format="%(levelname)s: %(message)s", level=log.DEBUG)

REPETITIONS = 1

# parameters, pag 203, Jakub thesis

# increasing just the number of vm
VM_AMOUNT = range(6,31,2)
MYSQL_PROV = [ 3 ]
MYSQL_REQ = [ 4 ]
WP_REQ = [ 4 ]

# doing a grid search
VM_AMOUNT = range(6,19,2)
MYSQL_PROV = [ 3 ]
MYSQL_REQ = range(4,11,2)
WP_REQ = range(4,11,2)

TIMEOUT = 120

# OTHER_OPT for zepyrus. Requires -v for checking the optimal value
OTHER_OPT = [ '-v']
# OTHER_OPT = [ '-v', '-S' ]

# check if solution is optimal and equal to [ OPT ]
# CHECK_OPT = True
CHECK_OPT = False
OPT = '390, 11'

CMDS = [
          # smt
          ['time', '-p', 'timeout', str(TIMEOUT),
          'python', '../zephyrus2.py',
          '-s', 'smt'],
        
          # ortools
          ['time', '-p', 'timeout', str(TIMEOUT),
          'python', '../zephyrus2.py',
          '-s', 'minisearch-ortools' ],

          # minisearch-chuffed
          ['time', '-p', 'timeout', str(TIMEOUT),
          'python', '../zephyrus2.py',
          '-s', 'minisearch-chuffed'],

          # minisearch-gecode
          ['time', '-p', 'timeout', str(TIMEOUT),
          'python', '../zephyrus2.py',
          '-s', 'minisearch-gecode' ],

          # gecode lex
          ['time', '-p', 'timeout', str(TIMEOUT),
          'python', '../zephyrus2.py',
          '-s', 'lex-gecode'],

          # ortools lex
          ['time', '-p', 'timeout', str(TIMEOUT),
          'python', '../zephyrus2.py',
          '-s', 'lex-ortools'],

          # chuffed lex
          ['time', '-p', 'timeout', str(TIMEOUT),
          'python', '../zephyrus2.py',
          '-s', 'lex-chuffed'],

          # smt-and-lex-gecode
          ['time', '-p', 'timeout', str(TIMEOUT),
          'python', '../zephyrus2.py',
          '-s', 'smt,lex-gecode'],

          # smt-and-lex-gecode-and-lex-chuffed
          ['time', '-p', 'timeout', str(TIMEOUT),
          'python', '../zephyrus2.py',
          '-s', 'smt,lex-gecode,lex-chuffed'],

               
        ]


EXECUTE = [ True for _ in CMDS ]
EXIT_AFTER_ERROR = False
EXIT_AFTER_NON_OPT = False

def read_json(json_file): 
  json_data = open(json_file)
  data = json.load(json_data)
  json_data.close()
  return data


def run_cmd(cmd):
  log.info("Running command: "  + str.join(' ',cmd))
  proc = Popen( cmd, stdout=PIPE, stderr=PIPE )
  out, err = proc.communicate()
  log.debug('Stdout')
  log.debug(out)
  log.debug('Stderr')
  log.debug(err)
  returncode = proc.returncode
  log.debug('Return code = ' + str(returncode))
  
  if returncode != 0:
    return (-float(returncode), '')
  else:
    obj = re.findall('Find better solution with obj value: \[(.*)\]\n',err)
    if CHECK_OPT and obj[-1] != OPT:
      return (float('NaN'),obj)
    else:
      return (float(re.search('real\s*([0-9\.]+)',err).group(1)),obj)    


def run(cmd):
  times = []
  errors = []
  non_opt = []
  objs = []
  for _ in range(REPETITIONS):
    time, obj = run_cmd(cmd)
    if obj:
      objs.append(obj[-1])
    if time < 0.0:
      log.debug('Timeout or error')
      if EXIT_AFTER_ERROR:
        exit(1)
      errors.append(time)
      break
    elif math.isnan(time):
      log.debug('Non optimal value found')
      if EXIT_AFTER_NON_OPT:
        exit(1)
      non_opt.append(time)
      break
    else:
      times.append(time)
  
  opt_eq = True
  if len(objs) > 1:
    for i in range(1,len(objs)):
      opt_eq = opt_eq and objs[0] == objs[i]       
  if len(non_opt) > 0 or (not opt_eq):
    return (0,0,0,len(non_opt), objs)
  elif len(errors) > 0:
    return (0,0, -errors[0],0, objs)    
  else:
    return (sum(times)/REPETITIONS, (max(times)- min(times))/2,
            0 , 0, objs)  


def myprint(res):
  if res[3] > 0:
    return("not-opt & not-opt")
  if res[2] > 0:
    return("error" + str(res[2]) + " & error" + str(res[2]))
  else:
    return(str(res[0]) + ' & ' + str(res[1]))
 
def main():
  
  # print first line of the table
  print 'vm_amount|mysqlreq|wpreq|mysqlprov ', 
  for i in CMDS:
    for j in range(len(i)):
      if i[j] == '-s':
        s = i[j+1]
        break
    print '& ' + s + ' & err',
  print '\\\\ % json input file'
  
  # start performing the grid search
  for vm_amount in VM_AMOUNT:
    for mysql_req in MYSQL_REQ:
      for wp_req in WP_REQ:
        for mysql_prov in MYSQL_PROV:
          
          # print the parameters of wordpress tested    
          print str(vm_amount) + '|' + str(mysql_req) + '|' + \
            str(wp_req) + '|' +str(mysql_prov) + ' & ', 
              
          # create the input file for zephyrus
          data = read_json('wordpress.json')
          for i in data["locations"].keys():
            data["locations"][i]['num'] = vm_amount
          data["components"]["wordpress"]["requires"]["mysql"] = mysql_req
          data["components"]["http_balancer"]["requires"]["wordpress"] = wp_req
          data["components"]["DNS_balancer"]["requires"]["wordpress"] = 2*wp_req + 1
          data["components"]["mysql"]["provides"][0]["num"] = mysql_prov
          json_file = "/tmp/" + uuid.uuid4().hex + ".json"
          open(json_file,'wb').write(json.dumps(data,indent=1))    
          
          # run the solvers
          obj = ''
          for i in range(len(CMDS)):
            if EXECUTE[i]:
              cmd = CMDS[i] + OTHER_OPT + [ json_file ]
              res = run(cmd)
              if res[3] > 0:
                EXECUTE[i] = False
              else:
                if res[4]:
                  if not obj:
                    obj = res[4][0]
                  if res[4][0] != obj:
                    log.critical('\nWarning: iteration ' + str(vm_amount) +
                        ' cmd ' + str.join(' ',cmd))
                    log.critical('  obj previously computed: ' + obj)
                    log.critical('  found instead: ' + res[4][0])
              print myprint(res),
              sys.stdout.flush()
            else:
              print '------- & --------',
            if i + 1 == len(CMDS):
              print '\\\\',
            else:
              print ' & ',
        print ' % '+ str(json_file)


main()   
