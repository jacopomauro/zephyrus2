# Generated from SpecificationGrammar.g4 by ANTLR 4.7
from antlr4 import *

# This class defines a complete generic visitor for a parse tree produced by SpecificationGrammarParser.

class SpecificationGrammarVisitor(ParseTreeVisitor):

    # Visit a parse tree produced by SpecificationGrammarParser#statement.
    def visitStatement(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SpecificationGrammarParser#ApreferenceCost.
    def visitApreferenceCost(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SpecificationGrammarParser#ApreferenceExpr.
    def visitApreferenceExpr(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SpecificationGrammarParser#b_expr.
    def visitB_expr(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SpecificationGrammarParser#b_term.
    def visitB_term(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SpecificationGrammarParser#b_factor.
    def visitB_factor(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SpecificationGrammarParser#relation.
    def visitRelation(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SpecificationGrammarParser#expr.
    def visitExpr(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SpecificationGrammarParser#AexprQuantifier.
    def visitAexprQuantifier(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SpecificationGrammarParser#AexprInt.
    def visitAexprInt(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SpecificationGrammarParser#AexprId.
    def visitAexprId(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SpecificationGrammarParser#AexprIDLoc.
    def visitAexprIDLoc(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SpecificationGrammarParser#AexprVariable.
    def visitAexprVariable(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SpecificationGrammarParser#AexprVariableId.
    def visitAexprVariableId(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SpecificationGrammarParser#AexprLocVariable.
    def visitAexprLocVariable(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SpecificationGrammarParser#AexprVariablevariable.
    def visitAexprVariablevariable(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SpecificationGrammarParser#AexprSum.
    def visitAexprSum(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SpecificationGrammarParser#AexprUnaryArithmetic.
    def visitAexprUnaryArithmetic(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SpecificationGrammarParser#AexprBrackets.
    def visitAexprBrackets(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SpecificationGrammarParser#typeV.
    def visitTypeV(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SpecificationGrammarParser#variable.
    def visitVariable(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SpecificationGrammarParser#bool_binary_op.
    def visitBool_binary_op(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SpecificationGrammarParser#arith_binary_op.
    def visitArith_binary_op(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SpecificationGrammarParser#arith_unary_op.
    def visitArith_unary_op(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SpecificationGrammarParser#comparison_op.
    def visitComparison_op(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SpecificationGrammarParser#unaryOp.
    def visitUnaryOp(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SpecificationGrammarParser#boolFact.
    def visitBoolFact(self, ctx):
        return self.visitChildren(ctx)


