/**
 * Copyright (c) 2016, Jacopo Mauro. All rights reserved. 
 * This file is licensed under the terms of the ISC License.
 */

grammar SpecificationGrammar;

// To generate files run antlr4 -Dlanguage=Python2 -visitor -no-listener


statement : b_expr (';' preference )* EOF;

preference: 
  COST                              # ApreferenceCost | 
  expr                              # ApreferenceExpr ;

b_expr : b_term (bool_binary_op b_term )* ;

b_term : (unaryOp)? b_factor ;

b_factor : boolFact | relation ; 

relation : expr (comparison_op expr)? ;

expr : term (arith_binary_op term)* ;
 
term :
  (EXISTS | FORALL) variable 'in' typeV ':' b_expr          # AexprQuantifier |
  INT                                                       # AexprInt |
  ID                                                        # AexprId |
  ID '[' INT ']' '.' ID                                     # AexprIDLoc |
  variable                                                  # AexprVariable | // variable of type component
  variable '.' ID                                           # AexprVariableId | // variable of type location
  ID '[' INT ']' '.' variable                               # AexprLocVariable |
  variable '.' variable                                     # AexprVariablevariable | // variable of type location and component
  SUM variable 'in' typeV ':' expr                          # AexprSum |
  arith_unary_op expr                                       # AexprUnaryArithmetic |
  '(' b_expr ')'                                            # AexprBrackets ;

typeV : LOCS | COMPS | RE ;

variable : VARIABLE ;
	
bool_binary_op : AND | OR | IMPL | IFF ;
arith_binary_op : PLUS | MINUS | TIMES ;
arith_unary_op : ABS ;

comparison_op : LEQ | EQ | GEQ | LT | GT | NEQ;
unaryOp : NOT;
boolFact : TRUE | FALSE;


RE : '\'' ([a-zA-Z0-9_] | '-' | '*' | '\\' | '+' | '?' | '[' | ']' | '|' | '.' |
  '^' | '$' | '{' | '}' | '(' | ') ' )+ '\'';   // match regular expression           

AND : 'and';
OR : 'or';
NOT : 'not';
TRUE : 'true';
FALSE : 'false';
IMPL: 'impl';
IFF: 'iff';
EXISTS: 'exists';
FORALL: 'forall';
SUM: 'sum';
COST: 'cost';

LEQ : '<=';
EQ : '=';
GEQ : '>=';
LT : '<';
GT : '>';
NEQ : '!=';

PLUS : '+';
MINUS : '-';
TIMES : '*';
ABS : 'abs';

COMPS: 'components';
LOCS: 'locations';

VARIABLE : '?'[a-zA-Z_][a-zA-Z0-9_]*;
      
ID : [a-zA-Z_][a-zA-Z0-9_]* ;    // match letters, numbers, underscore
INT : [0-9]+ ;
WS : [ \t\r\n]+ -> skip ; // skip spaces, tabs, newlines
