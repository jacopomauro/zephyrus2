# Generated from SpecificationGrammar.g4 by ANTLR 4.7
# encoding: utf-8
from __future__ import print_function
from antlr4 import *
from io import StringIO
import sys

def serializedATN():
    with StringIO() as buf:
        buf.write(u"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3")
        buf.write(u"&\u0090\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t")
        buf.write(u"\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r")
        buf.write(u"\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\3\2\3\2\3\2")
        buf.write(u"\7\2&\n\2\f\2\16\2)\13\2\3\2\3\2\3\3\3\3\5\3/\n\3\3\4")
        buf.write(u"\3\4\3\4\3\4\7\4\65\n\4\f\4\16\48\13\4\3\5\5\5;\n\5\3")
        buf.write(u"\5\3\5\3\6\3\6\5\6A\n\6\3\7\3\7\3\7\3\7\5\7G\n\7\3\b")
        buf.write(u"\3\b\3\b\3\b\7\bM\n\b\f\b\16\bP\13\b\3\t\3\t\3\t\3\t")
        buf.write(u"\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t")
        buf.write(u"\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t")
        buf.write(u"\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t")
        buf.write(u"\3\t\5\t~\n\t\3\n\3\n\3\13\3\13\3\f\3\f\3\r\3\r\3\16")
        buf.write(u"\3\16\3\17\3\17\3\20\3\20\3\21\3\21\3\21\2\2\22\2\4\6")
        buf.write(u"\b\n\f\16\20\22\24\26\30\32\34\36 \2\b\3\2\23\24\4\2")
        buf.write(u"\13\13!\"\4\2\f\r\21\22\3\2\35\37\3\2\27\34\3\2\17\20")
        buf.write(u"\2\u0090\2\"\3\2\2\2\4.\3\2\2\2\6\60\3\2\2\2\b:\3\2\2")
        buf.write(u"\2\n@\3\2\2\2\fB\3\2\2\2\16H\3\2\2\2\20}\3\2\2\2\22\177")
        buf.write(u"\3\2\2\2\24\u0081\3\2\2\2\26\u0083\3\2\2\2\30\u0085\3")
        buf.write(u"\2\2\2\32\u0087\3\2\2\2\34\u0089\3\2\2\2\36\u008b\3\2")
        buf.write(u"\2\2 \u008d\3\2\2\2\"\'\5\6\4\2#$\7\3\2\2$&\5\4\3\2%")
        buf.write(u"#\3\2\2\2&)\3\2\2\2\'%\3\2\2\2\'(\3\2\2\2(*\3\2\2\2)")
        buf.write(u"\'\3\2\2\2*+\7\2\2\3+\3\3\2\2\2,/\7\26\2\2-/\5\16\b\2")
        buf.write(u".,\3\2\2\2.-\3\2\2\2/\5\3\2\2\2\60\66\5\b\5\2\61\62\5")
        buf.write(u"\26\f\2\62\63\5\b\5\2\63\65\3\2\2\2\64\61\3\2\2\2\65")
        buf.write(u"8\3\2\2\2\66\64\3\2\2\2\66\67\3\2\2\2\67\7\3\2\2\28\66")
        buf.write(u"\3\2\2\29;\5\36\20\2:9\3\2\2\2:;\3\2\2\2;<\3\2\2\2<=")
        buf.write(u"\5\n\6\2=\t\3\2\2\2>A\5 \21\2?A\5\f\7\2@>\3\2\2\2@?\3")
        buf.write(u"\2\2\2A\13\3\2\2\2BF\5\16\b\2CD\5\34\17\2DE\5\16\b\2")
        buf.write(u"EG\3\2\2\2FC\3\2\2\2FG\3\2\2\2G\r\3\2\2\2HN\5\20\t\2")
        buf.write(u"IJ\5\30\r\2JK\5\20\t\2KM\3\2\2\2LI\3\2\2\2MP\3\2\2\2")
        buf.write(u"NL\3\2\2\2NO\3\2\2\2O\17\3\2\2\2PN\3\2\2\2QR\t\2\2\2")
        buf.write(u"RS\5\24\13\2ST\7\4\2\2TU\5\22\n\2UV\7\5\2\2VW\5\6\4\2")
        buf.write(u"W~\3\2\2\2X~\7%\2\2Y~\7$\2\2Z[\7$\2\2[\\\7\6\2\2\\]\7")
        buf.write(u"%\2\2]^\7\7\2\2^_\7\b\2\2_~\7$\2\2`~\5\24\13\2ab\5\24")
        buf.write(u"\13\2bc\7\b\2\2cd\7$\2\2d~\3\2\2\2ef\7$\2\2fg\7\6\2\2")
        buf.write(u"gh\7%\2\2hi\7\7\2\2ij\7\b\2\2j~\5\24\13\2kl\5\24\13\2")
        buf.write(u"lm\7\b\2\2mn\5\24\13\2n~\3\2\2\2op\7\25\2\2pq\5\24\13")
        buf.write(u"\2qr\7\4\2\2rs\5\22\n\2st\7\5\2\2tu\5\16\b\2u~\3\2\2")
        buf.write(u"\2vw\5\32\16\2wx\5\16\b\2x~\3\2\2\2yz\7\t\2\2z{\5\6\4")
        buf.write(u"\2{|\7\n\2\2|~\3\2\2\2}Q\3\2\2\2}X\3\2\2\2}Y\3\2\2\2")
        buf.write(u"}Z\3\2\2\2}`\3\2\2\2}a\3\2\2\2}e\3\2\2\2}k\3\2\2\2}o")
        buf.write(u"\3\2\2\2}v\3\2\2\2}y\3\2\2\2~\21\3\2\2\2\177\u0080\t")
        buf.write(u"\3\2\2\u0080\23\3\2\2\2\u0081\u0082\7#\2\2\u0082\25\3")
        buf.write(u"\2\2\2\u0083\u0084\t\4\2\2\u0084\27\3\2\2\2\u0085\u0086")
        buf.write(u"\t\5\2\2\u0086\31\3\2\2\2\u0087\u0088\7 \2\2\u0088\33")
        buf.write(u"\3\2\2\2\u0089\u008a\t\6\2\2\u008a\35\3\2\2\2\u008b\u008c")
        buf.write(u"\7\16\2\2\u008c\37\3\2\2\2\u008d\u008e\t\7\2\2\u008e")
        buf.write(u"!\3\2\2\2\n\'.\66:@FN}")
        return buf.getvalue()


class SpecificationGrammarParser ( Parser ):

    grammarFileName = "SpecificationGrammar.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ u"<INVALID>", u"';'", u"'in'", u"':'", u"'['", u"']'", 
                     u"'.'", u"'('", u"')'", u"<INVALID>", u"'and'", u"'or'", 
                     u"'not'", u"'true'", u"'false'", u"'impl'", u"'iff'", 
                     u"'exists'", u"'forall'", u"'sum'", u"'cost'", u"'<='", 
                     u"'='", u"'>='", u"'<'", u"'>'", u"'!='", u"'+'", u"'-'", 
                     u"'*'", u"'abs'", u"'components'", u"'locations'" ]

    symbolicNames = [ u"<INVALID>", u"<INVALID>", u"<INVALID>", u"<INVALID>", 
                      u"<INVALID>", u"<INVALID>", u"<INVALID>", u"<INVALID>", 
                      u"<INVALID>", u"RE", u"AND", u"OR", u"NOT", u"TRUE", 
                      u"FALSE", u"IMPL", u"IFF", u"EXISTS", u"FORALL", u"SUM", 
                      u"COST", u"LEQ", u"EQ", u"GEQ", u"LT", u"GT", u"NEQ", 
                      u"PLUS", u"MINUS", u"TIMES", u"ABS", u"COMPS", u"LOCS", 
                      u"VARIABLE", u"ID", u"INT", u"WS" ]

    RULE_statement = 0
    RULE_preference = 1
    RULE_b_expr = 2
    RULE_b_term = 3
    RULE_b_factor = 4
    RULE_relation = 5
    RULE_expr = 6
    RULE_term = 7
    RULE_typeV = 8
    RULE_variable = 9
    RULE_bool_binary_op = 10
    RULE_arith_binary_op = 11
    RULE_arith_unary_op = 12
    RULE_comparison_op = 13
    RULE_unaryOp = 14
    RULE_boolFact = 15

    ruleNames =  [ u"statement", u"preference", u"b_expr", u"b_term", u"b_factor", 
                   u"relation", u"expr", u"term", u"typeV", u"variable", 
                   u"bool_binary_op", u"arith_binary_op", u"arith_unary_op", 
                   u"comparison_op", u"unaryOp", u"boolFact" ]

    EOF = Token.EOF
    T__0=1
    T__1=2
    T__2=3
    T__3=4
    T__4=5
    T__5=6
    T__6=7
    T__7=8
    RE=9
    AND=10
    OR=11
    NOT=12
    TRUE=13
    FALSE=14
    IMPL=15
    IFF=16
    EXISTS=17
    FORALL=18
    SUM=19
    COST=20
    LEQ=21
    EQ=22
    GEQ=23
    LT=24
    GT=25
    NEQ=26
    PLUS=27
    MINUS=28
    TIMES=29
    ABS=30
    COMPS=31
    LOCS=32
    VARIABLE=33
    ID=34
    INT=35
    WS=36

    def __init__(self, input, output=sys.stdout):
        super(SpecificationGrammarParser, self).__init__(input, output=output)
        self.checkVersion("4.7")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None



    class StatementContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(SpecificationGrammarParser.StatementContext, self).__init__(parent, invokingState)
            self.parser = parser

        def b_expr(self):
            return self.getTypedRuleContext(SpecificationGrammarParser.B_exprContext,0)


        def EOF(self):
            return self.getToken(SpecificationGrammarParser.EOF, 0)

        def preference(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(SpecificationGrammarParser.PreferenceContext)
            else:
                return self.getTypedRuleContext(SpecificationGrammarParser.PreferenceContext,i)


        def getRuleIndex(self):
            return SpecificationGrammarParser.RULE_statement

        def accept(self, visitor):
            if hasattr(visitor, "visitStatement"):
                return visitor.visitStatement(self)
            else:
                return visitor.visitChildren(self)




    def statement(self):

        localctx = SpecificationGrammarParser.StatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_statement)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 32
            self.b_expr()
            self.state = 37
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==SpecificationGrammarParser.T__0:
                self.state = 33
                self.match(SpecificationGrammarParser.T__0)
                self.state = 34
                self.preference()
                self.state = 39
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 40
            self.match(SpecificationGrammarParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class PreferenceContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(SpecificationGrammarParser.PreferenceContext, self).__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return SpecificationGrammarParser.RULE_preference

     
        def copyFrom(self, ctx):
            super(SpecificationGrammarParser.PreferenceContext, self).copyFrom(ctx)



    class ApreferenceExprContext(PreferenceContext):

        def __init__(self, parser, ctx): # actually a SpecificationGrammarParser.PreferenceContext)
            super(SpecificationGrammarParser.ApreferenceExprContext, self).__init__(parser)
            self.copyFrom(ctx)

        def expr(self):
            return self.getTypedRuleContext(SpecificationGrammarParser.ExprContext,0)


        def accept(self, visitor):
            if hasattr(visitor, "visitApreferenceExpr"):
                return visitor.visitApreferenceExpr(self)
            else:
                return visitor.visitChildren(self)


    class ApreferenceCostContext(PreferenceContext):

        def __init__(self, parser, ctx): # actually a SpecificationGrammarParser.PreferenceContext)
            super(SpecificationGrammarParser.ApreferenceCostContext, self).__init__(parser)
            self.copyFrom(ctx)

        def COST(self):
            return self.getToken(SpecificationGrammarParser.COST, 0)

        def accept(self, visitor):
            if hasattr(visitor, "visitApreferenceCost"):
                return visitor.visitApreferenceCost(self)
            else:
                return visitor.visitChildren(self)



    def preference(self):

        localctx = SpecificationGrammarParser.PreferenceContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_preference)
        try:
            self.state = 44
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [SpecificationGrammarParser.COST]:
                localctx = SpecificationGrammarParser.ApreferenceCostContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 42
                self.match(SpecificationGrammarParser.COST)
                pass
            elif token in [SpecificationGrammarParser.T__6, SpecificationGrammarParser.EXISTS, SpecificationGrammarParser.FORALL, SpecificationGrammarParser.SUM, SpecificationGrammarParser.ABS, SpecificationGrammarParser.VARIABLE, SpecificationGrammarParser.ID, SpecificationGrammarParser.INT]:
                localctx = SpecificationGrammarParser.ApreferenceExprContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 43
                self.expr()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class B_exprContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(SpecificationGrammarParser.B_exprContext, self).__init__(parent, invokingState)
            self.parser = parser

        def b_term(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(SpecificationGrammarParser.B_termContext)
            else:
                return self.getTypedRuleContext(SpecificationGrammarParser.B_termContext,i)


        def bool_binary_op(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(SpecificationGrammarParser.Bool_binary_opContext)
            else:
                return self.getTypedRuleContext(SpecificationGrammarParser.Bool_binary_opContext,i)


        def getRuleIndex(self):
            return SpecificationGrammarParser.RULE_b_expr

        def accept(self, visitor):
            if hasattr(visitor, "visitB_expr"):
                return visitor.visitB_expr(self)
            else:
                return visitor.visitChildren(self)




    def b_expr(self):

        localctx = SpecificationGrammarParser.B_exprContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_b_expr)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 46
            self.b_term()
            self.state = 52
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,2,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 47
                    self.bool_binary_op()
                    self.state = 48
                    self.b_term() 
                self.state = 54
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,2,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class B_termContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(SpecificationGrammarParser.B_termContext, self).__init__(parent, invokingState)
            self.parser = parser

        def b_factor(self):
            return self.getTypedRuleContext(SpecificationGrammarParser.B_factorContext,0)


        def unaryOp(self):
            return self.getTypedRuleContext(SpecificationGrammarParser.UnaryOpContext,0)


        def getRuleIndex(self):
            return SpecificationGrammarParser.RULE_b_term

        def accept(self, visitor):
            if hasattr(visitor, "visitB_term"):
                return visitor.visitB_term(self)
            else:
                return visitor.visitChildren(self)




    def b_term(self):

        localctx = SpecificationGrammarParser.B_termContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_b_term)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 56
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==SpecificationGrammarParser.NOT:
                self.state = 55
                self.unaryOp()


            self.state = 58
            self.b_factor()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class B_factorContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(SpecificationGrammarParser.B_factorContext, self).__init__(parent, invokingState)
            self.parser = parser

        def boolFact(self):
            return self.getTypedRuleContext(SpecificationGrammarParser.BoolFactContext,0)


        def relation(self):
            return self.getTypedRuleContext(SpecificationGrammarParser.RelationContext,0)


        def getRuleIndex(self):
            return SpecificationGrammarParser.RULE_b_factor

        def accept(self, visitor):
            if hasattr(visitor, "visitB_factor"):
                return visitor.visitB_factor(self)
            else:
                return visitor.visitChildren(self)




    def b_factor(self):

        localctx = SpecificationGrammarParser.B_factorContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_b_factor)
        try:
            self.state = 62
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [SpecificationGrammarParser.TRUE, SpecificationGrammarParser.FALSE]:
                self.enterOuterAlt(localctx, 1)
                self.state = 60
                self.boolFact()
                pass
            elif token in [SpecificationGrammarParser.T__6, SpecificationGrammarParser.EXISTS, SpecificationGrammarParser.FORALL, SpecificationGrammarParser.SUM, SpecificationGrammarParser.ABS, SpecificationGrammarParser.VARIABLE, SpecificationGrammarParser.ID, SpecificationGrammarParser.INT]:
                self.enterOuterAlt(localctx, 2)
                self.state = 61
                self.relation()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class RelationContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(SpecificationGrammarParser.RelationContext, self).__init__(parent, invokingState)
            self.parser = parser

        def expr(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(SpecificationGrammarParser.ExprContext)
            else:
                return self.getTypedRuleContext(SpecificationGrammarParser.ExprContext,i)


        def comparison_op(self):
            return self.getTypedRuleContext(SpecificationGrammarParser.Comparison_opContext,0)


        def getRuleIndex(self):
            return SpecificationGrammarParser.RULE_relation

        def accept(self, visitor):
            if hasattr(visitor, "visitRelation"):
                return visitor.visitRelation(self)
            else:
                return visitor.visitChildren(self)




    def relation(self):

        localctx = SpecificationGrammarParser.RelationContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_relation)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 64
            self.expr()
            self.state = 68
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,5,self._ctx)
            if la_ == 1:
                self.state = 65
                self.comparison_op()
                self.state = 66
                self.expr()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ExprContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(SpecificationGrammarParser.ExprContext, self).__init__(parent, invokingState)
            self.parser = parser

        def term(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(SpecificationGrammarParser.TermContext)
            else:
                return self.getTypedRuleContext(SpecificationGrammarParser.TermContext,i)


        def arith_binary_op(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(SpecificationGrammarParser.Arith_binary_opContext)
            else:
                return self.getTypedRuleContext(SpecificationGrammarParser.Arith_binary_opContext,i)


        def getRuleIndex(self):
            return SpecificationGrammarParser.RULE_expr

        def accept(self, visitor):
            if hasattr(visitor, "visitExpr"):
                return visitor.visitExpr(self)
            else:
                return visitor.visitChildren(self)




    def expr(self):

        localctx = SpecificationGrammarParser.ExprContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_expr)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 70
            self.term()
            self.state = 76
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,6,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 71
                    self.arith_binary_op()
                    self.state = 72
                    self.term() 
                self.state = 78
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,6,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class TermContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(SpecificationGrammarParser.TermContext, self).__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return SpecificationGrammarParser.RULE_term

     
        def copyFrom(self, ctx):
            super(SpecificationGrammarParser.TermContext, self).copyFrom(ctx)



    class AexprVariableIdContext(TermContext):

        def __init__(self, parser, ctx): # actually a SpecificationGrammarParser.TermContext)
            super(SpecificationGrammarParser.AexprVariableIdContext, self).__init__(parser)
            self.copyFrom(ctx)

        def variable(self):
            return self.getTypedRuleContext(SpecificationGrammarParser.VariableContext,0)

        def ID(self):
            return self.getToken(SpecificationGrammarParser.ID, 0)

        def accept(self, visitor):
            if hasattr(visitor, "visitAexprVariableId"):
                return visitor.visitAexprVariableId(self)
            else:
                return visitor.visitChildren(self)


    class AexprQuantifierContext(TermContext):

        def __init__(self, parser, ctx): # actually a SpecificationGrammarParser.TermContext)
            super(SpecificationGrammarParser.AexprQuantifierContext, self).__init__(parser)
            self.copyFrom(ctx)

        def variable(self):
            return self.getTypedRuleContext(SpecificationGrammarParser.VariableContext,0)

        def typeV(self):
            return self.getTypedRuleContext(SpecificationGrammarParser.TypeVContext,0)

        def b_expr(self):
            return self.getTypedRuleContext(SpecificationGrammarParser.B_exprContext,0)

        def EXISTS(self):
            return self.getToken(SpecificationGrammarParser.EXISTS, 0)
        def FORALL(self):
            return self.getToken(SpecificationGrammarParser.FORALL, 0)

        def accept(self, visitor):
            if hasattr(visitor, "visitAexprQuantifier"):
                return visitor.visitAexprQuantifier(self)
            else:
                return visitor.visitChildren(self)


    class AexprSumContext(TermContext):

        def __init__(self, parser, ctx): # actually a SpecificationGrammarParser.TermContext)
            super(SpecificationGrammarParser.AexprSumContext, self).__init__(parser)
            self.copyFrom(ctx)

        def SUM(self):
            return self.getToken(SpecificationGrammarParser.SUM, 0)
        def variable(self):
            return self.getTypedRuleContext(SpecificationGrammarParser.VariableContext,0)

        def typeV(self):
            return self.getTypedRuleContext(SpecificationGrammarParser.TypeVContext,0)

        def expr(self):
            return self.getTypedRuleContext(SpecificationGrammarParser.ExprContext,0)


        def accept(self, visitor):
            if hasattr(visitor, "visitAexprSum"):
                return visitor.visitAexprSum(self)
            else:
                return visitor.visitChildren(self)


    class AexprLocVariableContext(TermContext):

        def __init__(self, parser, ctx): # actually a SpecificationGrammarParser.TermContext)
            super(SpecificationGrammarParser.AexprLocVariableContext, self).__init__(parser)
            self.copyFrom(ctx)

        def ID(self):
            return self.getToken(SpecificationGrammarParser.ID, 0)
        def INT(self):
            return self.getToken(SpecificationGrammarParser.INT, 0)
        def variable(self):
            return self.getTypedRuleContext(SpecificationGrammarParser.VariableContext,0)


        def accept(self, visitor):
            if hasattr(visitor, "visitAexprLocVariable"):
                return visitor.visitAexprLocVariable(self)
            else:
                return visitor.visitChildren(self)


    class AexprIDLocContext(TermContext):

        def __init__(self, parser, ctx): # actually a SpecificationGrammarParser.TermContext)
            super(SpecificationGrammarParser.AexprIDLocContext, self).__init__(parser)
            self.copyFrom(ctx)

        def ID(self, i=None):
            if i is None:
                return self.getTokens(SpecificationGrammarParser.ID)
            else:
                return self.getToken(SpecificationGrammarParser.ID, i)
        def INT(self):
            return self.getToken(SpecificationGrammarParser.INT, 0)

        def accept(self, visitor):
            if hasattr(visitor, "visitAexprIDLoc"):
                return visitor.visitAexprIDLoc(self)
            else:
                return visitor.visitChildren(self)


    class AexprVariablevariableContext(TermContext):

        def __init__(self, parser, ctx): # actually a SpecificationGrammarParser.TermContext)
            super(SpecificationGrammarParser.AexprVariablevariableContext, self).__init__(parser)
            self.copyFrom(ctx)

        def variable(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(SpecificationGrammarParser.VariableContext)
            else:
                return self.getTypedRuleContext(SpecificationGrammarParser.VariableContext,i)


        def accept(self, visitor):
            if hasattr(visitor, "visitAexprVariablevariable"):
                return visitor.visitAexprVariablevariable(self)
            else:
                return visitor.visitChildren(self)


    class AexprBracketsContext(TermContext):

        def __init__(self, parser, ctx): # actually a SpecificationGrammarParser.TermContext)
            super(SpecificationGrammarParser.AexprBracketsContext, self).__init__(parser)
            self.copyFrom(ctx)

        def b_expr(self):
            return self.getTypedRuleContext(SpecificationGrammarParser.B_exprContext,0)


        def accept(self, visitor):
            if hasattr(visitor, "visitAexprBrackets"):
                return visitor.visitAexprBrackets(self)
            else:
                return visitor.visitChildren(self)


    class AexprVariableContext(TermContext):

        def __init__(self, parser, ctx): # actually a SpecificationGrammarParser.TermContext)
            super(SpecificationGrammarParser.AexprVariableContext, self).__init__(parser)
            self.copyFrom(ctx)

        def variable(self):
            return self.getTypedRuleContext(SpecificationGrammarParser.VariableContext,0)


        def accept(self, visitor):
            if hasattr(visitor, "visitAexprVariable"):
                return visitor.visitAexprVariable(self)
            else:
                return visitor.visitChildren(self)


    class AexprUnaryArithmeticContext(TermContext):

        def __init__(self, parser, ctx): # actually a SpecificationGrammarParser.TermContext)
            super(SpecificationGrammarParser.AexprUnaryArithmeticContext, self).__init__(parser)
            self.copyFrom(ctx)

        def arith_unary_op(self):
            return self.getTypedRuleContext(SpecificationGrammarParser.Arith_unary_opContext,0)

        def expr(self):
            return self.getTypedRuleContext(SpecificationGrammarParser.ExprContext,0)


        def accept(self, visitor):
            if hasattr(visitor, "visitAexprUnaryArithmetic"):
                return visitor.visitAexprUnaryArithmetic(self)
            else:
                return visitor.visitChildren(self)


    class AexprIntContext(TermContext):

        def __init__(self, parser, ctx): # actually a SpecificationGrammarParser.TermContext)
            super(SpecificationGrammarParser.AexprIntContext, self).__init__(parser)
            self.copyFrom(ctx)

        def INT(self):
            return self.getToken(SpecificationGrammarParser.INT, 0)

        def accept(self, visitor):
            if hasattr(visitor, "visitAexprInt"):
                return visitor.visitAexprInt(self)
            else:
                return visitor.visitChildren(self)


    class AexprIdContext(TermContext):

        def __init__(self, parser, ctx): # actually a SpecificationGrammarParser.TermContext)
            super(SpecificationGrammarParser.AexprIdContext, self).__init__(parser)
            self.copyFrom(ctx)

        def ID(self):
            return self.getToken(SpecificationGrammarParser.ID, 0)

        def accept(self, visitor):
            if hasattr(visitor, "visitAexprId"):
                return visitor.visitAexprId(self)
            else:
                return visitor.visitChildren(self)



    def term(self):

        localctx = SpecificationGrammarParser.TermContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_term)
        self._la = 0 # Token type
        try:
            self.state = 123
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,7,self._ctx)
            if la_ == 1:
                localctx = SpecificationGrammarParser.AexprQuantifierContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 79
                _la = self._input.LA(1)
                if not(_la==SpecificationGrammarParser.EXISTS or _la==SpecificationGrammarParser.FORALL):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 80
                self.variable()
                self.state = 81
                self.match(SpecificationGrammarParser.T__1)
                self.state = 82
                self.typeV()
                self.state = 83
                self.match(SpecificationGrammarParser.T__2)
                self.state = 84
                self.b_expr()
                pass

            elif la_ == 2:
                localctx = SpecificationGrammarParser.AexprIntContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 86
                self.match(SpecificationGrammarParser.INT)
                pass

            elif la_ == 3:
                localctx = SpecificationGrammarParser.AexprIdContext(self, localctx)
                self.enterOuterAlt(localctx, 3)
                self.state = 87
                self.match(SpecificationGrammarParser.ID)
                pass

            elif la_ == 4:
                localctx = SpecificationGrammarParser.AexprIDLocContext(self, localctx)
                self.enterOuterAlt(localctx, 4)
                self.state = 88
                self.match(SpecificationGrammarParser.ID)
                self.state = 89
                self.match(SpecificationGrammarParser.T__3)
                self.state = 90
                self.match(SpecificationGrammarParser.INT)
                self.state = 91
                self.match(SpecificationGrammarParser.T__4)
                self.state = 92
                self.match(SpecificationGrammarParser.T__5)
                self.state = 93
                self.match(SpecificationGrammarParser.ID)
                pass

            elif la_ == 5:
                localctx = SpecificationGrammarParser.AexprVariableContext(self, localctx)
                self.enterOuterAlt(localctx, 5)
                self.state = 94
                self.variable()
                pass

            elif la_ == 6:
                localctx = SpecificationGrammarParser.AexprVariableIdContext(self, localctx)
                self.enterOuterAlt(localctx, 6)
                self.state = 95
                self.variable()
                self.state = 96
                self.match(SpecificationGrammarParser.T__5)
                self.state = 97
                self.match(SpecificationGrammarParser.ID)
                pass

            elif la_ == 7:
                localctx = SpecificationGrammarParser.AexprLocVariableContext(self, localctx)
                self.enterOuterAlt(localctx, 7)
                self.state = 99
                self.match(SpecificationGrammarParser.ID)
                self.state = 100
                self.match(SpecificationGrammarParser.T__3)
                self.state = 101
                self.match(SpecificationGrammarParser.INT)
                self.state = 102
                self.match(SpecificationGrammarParser.T__4)
                self.state = 103
                self.match(SpecificationGrammarParser.T__5)
                self.state = 104
                self.variable()
                pass

            elif la_ == 8:
                localctx = SpecificationGrammarParser.AexprVariablevariableContext(self, localctx)
                self.enterOuterAlt(localctx, 8)
                self.state = 105
                self.variable()
                self.state = 106
                self.match(SpecificationGrammarParser.T__5)
                self.state = 107
                self.variable()
                pass

            elif la_ == 9:
                localctx = SpecificationGrammarParser.AexprSumContext(self, localctx)
                self.enterOuterAlt(localctx, 9)
                self.state = 109
                self.match(SpecificationGrammarParser.SUM)
                self.state = 110
                self.variable()
                self.state = 111
                self.match(SpecificationGrammarParser.T__1)
                self.state = 112
                self.typeV()
                self.state = 113
                self.match(SpecificationGrammarParser.T__2)
                self.state = 114
                self.expr()
                pass

            elif la_ == 10:
                localctx = SpecificationGrammarParser.AexprUnaryArithmeticContext(self, localctx)
                self.enterOuterAlt(localctx, 10)
                self.state = 116
                self.arith_unary_op()
                self.state = 117
                self.expr()
                pass

            elif la_ == 11:
                localctx = SpecificationGrammarParser.AexprBracketsContext(self, localctx)
                self.enterOuterAlt(localctx, 11)
                self.state = 119
                self.match(SpecificationGrammarParser.T__6)
                self.state = 120
                self.b_expr()
                self.state = 121
                self.match(SpecificationGrammarParser.T__7)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class TypeVContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(SpecificationGrammarParser.TypeVContext, self).__init__(parent, invokingState)
            self.parser = parser

        def LOCS(self):
            return self.getToken(SpecificationGrammarParser.LOCS, 0)

        def COMPS(self):
            return self.getToken(SpecificationGrammarParser.COMPS, 0)

        def RE(self):
            return self.getToken(SpecificationGrammarParser.RE, 0)

        def getRuleIndex(self):
            return SpecificationGrammarParser.RULE_typeV

        def accept(self, visitor):
            if hasattr(visitor, "visitTypeV"):
                return visitor.visitTypeV(self)
            else:
                return visitor.visitChildren(self)




    def typeV(self):

        localctx = SpecificationGrammarParser.TypeVContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_typeV)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 125
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << SpecificationGrammarParser.RE) | (1 << SpecificationGrammarParser.COMPS) | (1 << SpecificationGrammarParser.LOCS))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class VariableContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(SpecificationGrammarParser.VariableContext, self).__init__(parent, invokingState)
            self.parser = parser

        def VARIABLE(self):
            return self.getToken(SpecificationGrammarParser.VARIABLE, 0)

        def getRuleIndex(self):
            return SpecificationGrammarParser.RULE_variable

        def accept(self, visitor):
            if hasattr(visitor, "visitVariable"):
                return visitor.visitVariable(self)
            else:
                return visitor.visitChildren(self)




    def variable(self):

        localctx = SpecificationGrammarParser.VariableContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_variable)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 127
            self.match(SpecificationGrammarParser.VARIABLE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Bool_binary_opContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(SpecificationGrammarParser.Bool_binary_opContext, self).__init__(parent, invokingState)
            self.parser = parser

        def AND(self):
            return self.getToken(SpecificationGrammarParser.AND, 0)

        def OR(self):
            return self.getToken(SpecificationGrammarParser.OR, 0)

        def IMPL(self):
            return self.getToken(SpecificationGrammarParser.IMPL, 0)

        def IFF(self):
            return self.getToken(SpecificationGrammarParser.IFF, 0)

        def getRuleIndex(self):
            return SpecificationGrammarParser.RULE_bool_binary_op

        def accept(self, visitor):
            if hasattr(visitor, "visitBool_binary_op"):
                return visitor.visitBool_binary_op(self)
            else:
                return visitor.visitChildren(self)




    def bool_binary_op(self):

        localctx = SpecificationGrammarParser.Bool_binary_opContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_bool_binary_op)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 129
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << SpecificationGrammarParser.AND) | (1 << SpecificationGrammarParser.OR) | (1 << SpecificationGrammarParser.IMPL) | (1 << SpecificationGrammarParser.IFF))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Arith_binary_opContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(SpecificationGrammarParser.Arith_binary_opContext, self).__init__(parent, invokingState)
            self.parser = parser

        def PLUS(self):
            return self.getToken(SpecificationGrammarParser.PLUS, 0)

        def MINUS(self):
            return self.getToken(SpecificationGrammarParser.MINUS, 0)

        def TIMES(self):
            return self.getToken(SpecificationGrammarParser.TIMES, 0)

        def getRuleIndex(self):
            return SpecificationGrammarParser.RULE_arith_binary_op

        def accept(self, visitor):
            if hasattr(visitor, "visitArith_binary_op"):
                return visitor.visitArith_binary_op(self)
            else:
                return visitor.visitChildren(self)




    def arith_binary_op(self):

        localctx = SpecificationGrammarParser.Arith_binary_opContext(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_arith_binary_op)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 131
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << SpecificationGrammarParser.PLUS) | (1 << SpecificationGrammarParser.MINUS) | (1 << SpecificationGrammarParser.TIMES))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Arith_unary_opContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(SpecificationGrammarParser.Arith_unary_opContext, self).__init__(parent, invokingState)
            self.parser = parser

        def ABS(self):
            return self.getToken(SpecificationGrammarParser.ABS, 0)

        def getRuleIndex(self):
            return SpecificationGrammarParser.RULE_arith_unary_op

        def accept(self, visitor):
            if hasattr(visitor, "visitArith_unary_op"):
                return visitor.visitArith_unary_op(self)
            else:
                return visitor.visitChildren(self)




    def arith_unary_op(self):

        localctx = SpecificationGrammarParser.Arith_unary_opContext(self, self._ctx, self.state)
        self.enterRule(localctx, 24, self.RULE_arith_unary_op)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 133
            self.match(SpecificationGrammarParser.ABS)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Comparison_opContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(SpecificationGrammarParser.Comparison_opContext, self).__init__(parent, invokingState)
            self.parser = parser

        def LEQ(self):
            return self.getToken(SpecificationGrammarParser.LEQ, 0)

        def EQ(self):
            return self.getToken(SpecificationGrammarParser.EQ, 0)

        def GEQ(self):
            return self.getToken(SpecificationGrammarParser.GEQ, 0)

        def LT(self):
            return self.getToken(SpecificationGrammarParser.LT, 0)

        def GT(self):
            return self.getToken(SpecificationGrammarParser.GT, 0)

        def NEQ(self):
            return self.getToken(SpecificationGrammarParser.NEQ, 0)

        def getRuleIndex(self):
            return SpecificationGrammarParser.RULE_comparison_op

        def accept(self, visitor):
            if hasattr(visitor, "visitComparison_op"):
                return visitor.visitComparison_op(self)
            else:
                return visitor.visitChildren(self)




    def comparison_op(self):

        localctx = SpecificationGrammarParser.Comparison_opContext(self, self._ctx, self.state)
        self.enterRule(localctx, 26, self.RULE_comparison_op)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 135
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << SpecificationGrammarParser.LEQ) | (1 << SpecificationGrammarParser.EQ) | (1 << SpecificationGrammarParser.GEQ) | (1 << SpecificationGrammarParser.LT) | (1 << SpecificationGrammarParser.GT) | (1 << SpecificationGrammarParser.NEQ))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class UnaryOpContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(SpecificationGrammarParser.UnaryOpContext, self).__init__(parent, invokingState)
            self.parser = parser

        def NOT(self):
            return self.getToken(SpecificationGrammarParser.NOT, 0)

        def getRuleIndex(self):
            return SpecificationGrammarParser.RULE_unaryOp

        def accept(self, visitor):
            if hasattr(visitor, "visitUnaryOp"):
                return visitor.visitUnaryOp(self)
            else:
                return visitor.visitChildren(self)




    def unaryOp(self):

        localctx = SpecificationGrammarParser.UnaryOpContext(self, self._ctx, self.state)
        self.enterRule(localctx, 28, self.RULE_unaryOp)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 137
            self.match(SpecificationGrammarParser.NOT)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class BoolFactContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(SpecificationGrammarParser.BoolFactContext, self).__init__(parent, invokingState)
            self.parser = parser

        def TRUE(self):
            return self.getToken(SpecificationGrammarParser.TRUE, 0)

        def FALSE(self):
            return self.getToken(SpecificationGrammarParser.FALSE, 0)

        def getRuleIndex(self):
            return SpecificationGrammarParser.RULE_boolFact

        def accept(self, visitor):
            if hasattr(visitor, "visitBoolFact"):
                return visitor.visitBoolFact(self)
            else:
                return visitor.visitChildren(self)




    def boolFact(self):

        localctx = SpecificationGrammarParser.BoolFactContext(self, self._ctx, self.state)
        self.enterRule(localctx, 30, self.RULE_boolFact)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 139
            _la = self._input.LA(1)
            if not(_la==SpecificationGrammarParser.TRUE or _la==SpecificationGrammarParser.FALSE):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





