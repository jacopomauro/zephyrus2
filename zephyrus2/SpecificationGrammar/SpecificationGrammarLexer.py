# Generated from SpecificationGrammar.g4 by ANTLR 4.7
# encoding: utf-8
from __future__ import print_function
from antlr4 import *
from io import StringIO
import sys


def serializedATN():
    with StringIO() as buf:
        buf.write(u"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2")
        buf.write(u"&\u00e5\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4")
        buf.write(u"\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r")
        buf.write(u"\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22")
        buf.write(u"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4")
        buf.write(u"\30\t\30\4\31\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35")
        buf.write(u"\t\35\4\36\t\36\4\37\t\37\4 \t \4!\t!\4\"\t\"\4#\t#\4")
        buf.write(u"$\t$\4%\t%\3\2\3\2\3\3\3\3\3\3\3\4\3\4\3\5\3\5\3\6\3")
        buf.write(u"\6\3\7\3\7\3\b\3\b\3\t\3\t\3\n\3\n\3\n\3\n\6\na\n\n\r")
        buf.write(u"\n\16\nb\3\n\3\n\3\13\3\13\3\13\3\13\3\f\3\f\3\f\3\r")
        buf.write(u"\3\r\3\r\3\r\3\16\3\16\3\16\3\16\3\16\3\17\3\17\3\17")
        buf.write(u"\3\17\3\17\3\17\3\20\3\20\3\20\3\20\3\20\3\21\3\21\3")
        buf.write(u"\21\3\21\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\23\3\23")
        buf.write(u"\3\23\3\23\3\23\3\23\3\23\3\24\3\24\3\24\3\24\3\25\3")
        buf.write(u"\25\3\25\3\25\3\25\3\26\3\26\3\26\3\27\3\27\3\30\3\30")
        buf.write(u"\3\30\3\31\3\31\3\32\3\32\3\33\3\33\3\33\3\34\3\34\3")
        buf.write(u"\35\3\35\3\36\3\36\3\37\3\37\3\37\3\37\3 \3 \3 \3 \3")
        buf.write(u" \3 \3 \3 \3 \3 \3 \3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3\"")
        buf.write(u"\3\"\3\"\7\"\u00ce\n\"\f\"\16\"\u00d1\13\"\3#\3#\7#\u00d5")
        buf.write(u"\n#\f#\16#\u00d8\13#\3$\6$\u00db\n$\r$\16$\u00dc\3%\6")
        buf.write(u"%\u00e0\n%\r%\16%\u00e1\3%\3%\2\2&\3\3\5\4\7\5\t\6\13")
        buf.write(u"\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17\35\20\37")
        buf.write(u"\21!\22#\23%\24\'\25)\26+\27-\30/\31\61\32\63\33\65\34")
        buf.write(u"\67\359\36;\37= ?!A\"C#E$G%I&\3\2\7\n\2&&**,-/\60\62")
        buf.write(u";AACac\177\5\2C\\aac|\6\2\62;C\\aac|\3\2\62;\5\2\13\f")
        buf.write(u"\17\17\"\"\2\u00ea\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2")
        buf.write(u"\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2")
        buf.write(u"\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2")
        buf.write(u"\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2")
        buf.write(u"\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2")
        buf.write(u"\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3\2\2\2\2\61\3\2\2\2\2")
        buf.write(u"\63\3\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2\29\3\2\2\2\2;\3")
        buf.write(u"\2\2\2\2=\3\2\2\2\2?\3\2\2\2\2A\3\2\2\2\2C\3\2\2\2\2")
        buf.write(u"E\3\2\2\2\2G\3\2\2\2\2I\3\2\2\2\3K\3\2\2\2\5M\3\2\2\2")
        buf.write(u"\7P\3\2\2\2\tR\3\2\2\2\13T\3\2\2\2\rV\3\2\2\2\17X\3\2")
        buf.write(u"\2\2\21Z\3\2\2\2\23\\\3\2\2\2\25f\3\2\2\2\27j\3\2\2\2")
        buf.write(u"\31m\3\2\2\2\33q\3\2\2\2\35v\3\2\2\2\37|\3\2\2\2!\u0081")
        buf.write(u"\3\2\2\2#\u0085\3\2\2\2%\u008c\3\2\2\2\'\u0093\3\2\2")
        buf.write(u"\2)\u0097\3\2\2\2+\u009c\3\2\2\2-\u009f\3\2\2\2/\u00a1")
        buf.write(u"\3\2\2\2\61\u00a4\3\2\2\2\63\u00a6\3\2\2\2\65\u00a8\3")
        buf.write(u"\2\2\2\67\u00ab\3\2\2\29\u00ad\3\2\2\2;\u00af\3\2\2\2")
        buf.write(u"=\u00b1\3\2\2\2?\u00b5\3\2\2\2A\u00c0\3\2\2\2C\u00ca")
        buf.write(u"\3\2\2\2E\u00d2\3\2\2\2G\u00da\3\2\2\2I\u00df\3\2\2\2")
        buf.write(u"KL\7=\2\2L\4\3\2\2\2MN\7k\2\2NO\7p\2\2O\6\3\2\2\2PQ\7")
        buf.write(u"<\2\2Q\b\3\2\2\2RS\7]\2\2S\n\3\2\2\2TU\7_\2\2U\f\3\2")
        buf.write(u"\2\2VW\7\60\2\2W\16\3\2\2\2XY\7*\2\2Y\20\3\2\2\2Z[\7")
        buf.write(u"+\2\2[\22\3\2\2\2\\`\7)\2\2]a\t\2\2\2^_\7+\2\2_a\7\"")
        buf.write(u"\2\2`]\3\2\2\2`^\3\2\2\2ab\3\2\2\2b`\3\2\2\2bc\3\2\2")
        buf.write(u"\2cd\3\2\2\2de\7)\2\2e\24\3\2\2\2fg\7c\2\2gh\7p\2\2h")
        buf.write(u"i\7f\2\2i\26\3\2\2\2jk\7q\2\2kl\7t\2\2l\30\3\2\2\2mn")
        buf.write(u"\7p\2\2no\7q\2\2op\7v\2\2p\32\3\2\2\2qr\7v\2\2rs\7t\2")
        buf.write(u"\2st\7w\2\2tu\7g\2\2u\34\3\2\2\2vw\7h\2\2wx\7c\2\2xy")
        buf.write(u"\7n\2\2yz\7u\2\2z{\7g\2\2{\36\3\2\2\2|}\7k\2\2}~\7o\2")
        buf.write(u"\2~\177\7r\2\2\177\u0080\7n\2\2\u0080 \3\2\2\2\u0081")
        buf.write(u"\u0082\7k\2\2\u0082\u0083\7h\2\2\u0083\u0084\7h\2\2\u0084")
        buf.write(u"\"\3\2\2\2\u0085\u0086\7g\2\2\u0086\u0087\7z\2\2\u0087")
        buf.write(u"\u0088\7k\2\2\u0088\u0089\7u\2\2\u0089\u008a\7v\2\2\u008a")
        buf.write(u"\u008b\7u\2\2\u008b$\3\2\2\2\u008c\u008d\7h\2\2\u008d")
        buf.write(u"\u008e\7q\2\2\u008e\u008f\7t\2\2\u008f\u0090\7c\2\2\u0090")
        buf.write(u"\u0091\7n\2\2\u0091\u0092\7n\2\2\u0092&\3\2\2\2\u0093")
        buf.write(u"\u0094\7u\2\2\u0094\u0095\7w\2\2\u0095\u0096\7o\2\2\u0096")
        buf.write(u"(\3\2\2\2\u0097\u0098\7e\2\2\u0098\u0099\7q\2\2\u0099")
        buf.write(u"\u009a\7u\2\2\u009a\u009b\7v\2\2\u009b*\3\2\2\2\u009c")
        buf.write(u"\u009d\7>\2\2\u009d\u009e\7?\2\2\u009e,\3\2\2\2\u009f")
        buf.write(u"\u00a0\7?\2\2\u00a0.\3\2\2\2\u00a1\u00a2\7@\2\2\u00a2")
        buf.write(u"\u00a3\7?\2\2\u00a3\60\3\2\2\2\u00a4\u00a5\7>\2\2\u00a5")
        buf.write(u"\62\3\2\2\2\u00a6\u00a7\7@\2\2\u00a7\64\3\2\2\2\u00a8")
        buf.write(u"\u00a9\7#\2\2\u00a9\u00aa\7?\2\2\u00aa\66\3\2\2\2\u00ab")
        buf.write(u"\u00ac\7-\2\2\u00ac8\3\2\2\2\u00ad\u00ae\7/\2\2\u00ae")
        buf.write(u":\3\2\2\2\u00af\u00b0\7,\2\2\u00b0<\3\2\2\2\u00b1\u00b2")
        buf.write(u"\7c\2\2\u00b2\u00b3\7d\2\2\u00b3\u00b4\7u\2\2\u00b4>")
        buf.write(u"\3\2\2\2\u00b5\u00b6\7e\2\2\u00b6\u00b7\7q\2\2\u00b7")
        buf.write(u"\u00b8\7o\2\2\u00b8\u00b9\7r\2\2\u00b9\u00ba\7q\2\2\u00ba")
        buf.write(u"\u00bb\7p\2\2\u00bb\u00bc\7g\2\2\u00bc\u00bd\7p\2\2\u00bd")
        buf.write(u"\u00be\7v\2\2\u00be\u00bf\7u\2\2\u00bf@\3\2\2\2\u00c0")
        buf.write(u"\u00c1\7n\2\2\u00c1\u00c2\7q\2\2\u00c2\u00c3\7e\2\2\u00c3")
        buf.write(u"\u00c4\7c\2\2\u00c4\u00c5\7v\2\2\u00c5\u00c6\7k\2\2\u00c6")
        buf.write(u"\u00c7\7q\2\2\u00c7\u00c8\7p\2\2\u00c8\u00c9\7u\2\2\u00c9")
        buf.write(u"B\3\2\2\2\u00ca\u00cb\7A\2\2\u00cb\u00cf\t\3\2\2\u00cc")
        buf.write(u"\u00ce\t\4\2\2\u00cd\u00cc\3\2\2\2\u00ce\u00d1\3\2\2")
        buf.write(u"\2\u00cf\u00cd\3\2\2\2\u00cf\u00d0\3\2\2\2\u00d0D\3\2")
        buf.write(u"\2\2\u00d1\u00cf\3\2\2\2\u00d2\u00d6\t\3\2\2\u00d3\u00d5")
        buf.write(u"\t\4\2\2\u00d4\u00d3\3\2\2\2\u00d5\u00d8\3\2\2\2\u00d6")
        buf.write(u"\u00d4\3\2\2\2\u00d6\u00d7\3\2\2\2\u00d7F\3\2\2\2\u00d8")
        buf.write(u"\u00d6\3\2\2\2\u00d9\u00db\t\5\2\2\u00da\u00d9\3\2\2")
        buf.write(u"\2\u00db\u00dc\3\2\2\2\u00dc\u00da\3\2\2\2\u00dc\u00dd")
        buf.write(u"\3\2\2\2\u00ddH\3\2\2\2\u00de\u00e0\t\6\2\2\u00df\u00de")
        buf.write(u"\3\2\2\2\u00e0\u00e1\3\2\2\2\u00e1\u00df\3\2\2\2\u00e1")
        buf.write(u"\u00e2\3\2\2\2\u00e2\u00e3\3\2\2\2\u00e3\u00e4\b%\2\2")
        buf.write(u"\u00e4J\3\2\2\2\t\2`b\u00cf\u00d6\u00dc\u00e1\3\b\2\2")
        return buf.getvalue()


class SpecificationGrammarLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    T__0 = 1
    T__1 = 2
    T__2 = 3
    T__3 = 4
    T__4 = 5
    T__5 = 6
    T__6 = 7
    T__7 = 8
    RE = 9
    AND = 10
    OR = 11
    NOT = 12
    TRUE = 13
    FALSE = 14
    IMPL = 15
    IFF = 16
    EXISTS = 17
    FORALL = 18
    SUM = 19
    COST = 20
    LEQ = 21
    EQ = 22
    GEQ = 23
    LT = 24
    GT = 25
    NEQ = 26
    PLUS = 27
    MINUS = 28
    TIMES = 29
    ABS = 30
    COMPS = 31
    LOCS = 32
    VARIABLE = 33
    ID = 34
    INT = 35
    WS = 36

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ u"DEFAULT_MODE" ]

    literalNames = [ u"<INVALID>",
            u"';'", u"'in'", u"':'", u"'['", u"']'", u"'.'", u"'('", u"')'", 
            u"'and'", u"'or'", u"'not'", u"'true'", u"'false'", u"'impl'", 
            u"'iff'", u"'exists'", u"'forall'", u"'sum'", u"'cost'", u"'<='", 
            u"'='", u"'>='", u"'<'", u"'>'", u"'!='", u"'+'", u"'-'", u"'*'", 
            u"'abs'", u"'components'", u"'locations'" ]

    symbolicNames = [ u"<INVALID>",
            u"RE", u"AND", u"OR", u"NOT", u"TRUE", u"FALSE", u"IMPL", u"IFF", 
            u"EXISTS", u"FORALL", u"SUM", u"COST", u"LEQ", u"EQ", u"GEQ", 
            u"LT", u"GT", u"NEQ", u"PLUS", u"MINUS", u"TIMES", u"ABS", u"COMPS", 
            u"LOCS", u"VARIABLE", u"ID", u"INT", u"WS" ]

    ruleNames = [ u"T__0", u"T__1", u"T__2", u"T__3", u"T__4", u"T__5", 
                  u"T__6", u"T__7", u"RE", u"AND", u"OR", u"NOT", u"TRUE", 
                  u"FALSE", u"IMPL", u"IFF", u"EXISTS", u"FORALL", u"SUM", 
                  u"COST", u"LEQ", u"EQ", u"GEQ", u"LT", u"GT", u"NEQ", 
                  u"PLUS", u"MINUS", u"TIMES", u"ABS", u"COMPS", u"LOCS", 
                  u"VARIABLE", u"ID", u"INT", u"WS" ]

    grammarFileName = u"SpecificationGrammar.g4"

    def __init__(self, input=None, output=sys.stdout):
        super(SpecificationGrammarLexer, self).__init__(input, output=output)
        self.checkVersion("4.7")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


