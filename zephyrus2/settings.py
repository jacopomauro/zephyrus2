""" A collection of variables used by the various modules """
__author__ = "Jacopo Mauro"
__copyright__ = "Copyright 2016, Jacopo Mauro"
__license__ = "ISC"
__version__ = "0.1"
__maintainer__ = "Jacopo Mauro"
__email__ = "mauro.jacopo@gmail.com"
__status__ = "Prototype"

import threading
import uuid
import logging as log

MINISEARCH_COMMAND = "minisearch"

MZN_STDLIB_PATH = ""

SEPARATOR = "___"

SLEEP_TIME = 1

# List of the temp files.
TMP_FILES = []
TMP_FILES_LOCK = threading.Lock()


def get_new_temp_file(extension):
  global TMP_FILES_LOCK
  global TMP_FILES
  name = '/tmp/' + uuid.uuid4().hex + '.' + extension
  TMP_FILES_LOCK.acquire()
  TMP_FILES.append(name)
  TMP_FILES_LOCK.release()
  return name

def validate_input_json(data):
  """
  Validate the json string, filling eventually with some default values if needed
  :param data: the json string to check
  :return: the json string validated
  """

  if "specification" not in data:
    raise ValueError("Specification must be defined")

  if "locations" not in data or not data["locations"]:
    raise ValueError("At least one location must be defined")

  if "components" not in data or not data["components"]:
    raise ValueError("At least one component must be defined")

  for i in data["locations"]:
    obj = data["locations"][i]

    if "num" not in obj:
      log.warning("Num not found for location {}, set it to default value 1".format(i))
      obj["num"] = 1
    if "cost" not in obj:
      log.warning("Cost not found for location {}, set it to default value 1".format(i))
      obj["cost"] = 1
    if "resources" not in obj:
      obj["resources"] = {("res_" + unicode(uuid.uuid4().hex)): 0}


  for i in data["components"]:
    obj = data["components"][i]
    # add resource if not present
    if "resources" not in obj:
      obj["resources"] = {}
    # add fictional provide if provides is not present or empty
    if "provides" not in obj:
      obj["provides"] = [ { "ports": [ "funct_" + unicode(uuid.uuid4().hex) ], "num": -1 }]
    if not obj["provides"]:
      obj["provides"] = [{"ports": ["funct_" + unicode(uuid.uuid4().hex)], "num": -1}]
    for j in obj["provides"]:
      if "num" not in j:
        j["num"] = -1

    if "conflicts" not in obj:
      obj["conflicts"] = []
    if "requires" not in obj:
      obj["requires"] = {}
  return data


