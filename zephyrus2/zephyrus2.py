"""
Usage: zephyrus2.py [<options>] <json description file>
  Options:
    -o <file>, --ofile <file>
      file where to save the output
    -v, --verbose
      print debug messages 
    -k, --keep
      keep the tmp files (default remove)
    -s <solver>, --solver <solver>
      the solving approach to use. See below for the supported solvers.
    -S, --no-simmetry-breaking
      do not use symmetry breaking constraints. This option must be used when
      user has preferences over the locations having similar resources
    -c, --check-sol
      check if the solution is optimal (default no checking)
    -G <dir>, --globals-dir  <dir>
      minisearch global directory override when only one solver is selected
    -f <cmd>, --flatzinc-cmd <cmd>
      fzn solver override to use when only one solver is selected
"""
__author__ = "Jacopo Mauro"
__copyright__ = "Copyright 2016, Jacopo Mauro"
__license__ = "ISC"
__version__ = "0.1"
__maintainer__ = "Jacopo Mauro"
__email__ = "mauro.jacopo@gmail.com"
__status__ = "Prototype"


import json
from subprocess import Popen,PIPE
import sys, getopt
import os
import logging as log
import shutil
import signal
import time



from antlr4 import InputStream

import settings
import spec_translator
from doctest import SKIP
from solver import Solver

DEVNULL = open(os.devnull, 'wb')

# If KEEP, don't delete temporary files.
KEEP = False

# List of the running solvers.
RUNNING_SOLVERS = []

# Check with gecode if the solution is really the optimal one
CHECK_SOLUTION = False

SOLVERS = {
  'smt' : ['smt', 'metric_satisfy',
      {}, "" ],
  'lex-gecode' : ["lex", '',
      { '--globals-dir' : 'gecode', 
        '--solver': 'fzn-gecode'},
      """:: seq_search([
              int_search([used_locations[costs_desc[i]] | i in locations], 
                input_order, indomain_min, complete),
              int_search([comp_locations[l, i] | l in locations, i in comps], 
                first_fail, indomain_max, complete),
              int_search(comps_num, first_fail, indomain_max, complete),
              int_search([bindings[m,p,i,j] | m in multi_provide_ports, p in ports, i,j in comps], 
                first_fail, indomain_max, complete)
              ]) """,],
  'lex-or-tools' : ["lex", '',
      { '--globals-dir' : 'or-tools',
        '--solver': 'fzn-or-tools'},
      """:: seq_search([
              int_search([used_locations[costs_desc[i]] | i in locations], 
                input_order, indomain_min, complete),
              int_search([comp_locations[l, i] | l in locations, i in comps], 
                first_fail, indomain_max, complete),
              int_search(comps_num, first_fail, indomain_max, complete),
              int_search([bindings[m,p,i,j] | m in multi_provide_ports, p in ports, i,j in comps], 
                first_fail, indomain_max, complete)
              ]) """,],
  'lex-chuffed' : ["lex", '', 
      { '--globals-dir' : 'chuffed',
        '--solver': 'fzn-chuffed'},
      """:: seq_search([
              int_search([used_locations[costs_desc[i]] | i in locations], 
                input_order, indomain_min, complete),
              int_search([comp_locations[l, i] | l in locations, i in comps], 
                first_fail, indomain_max, complete),
              int_search(comps_num, first_fail, indomain_max, complete),
              int_search([bindings[m,p,i,j] | m in multi_provide_ports, p in ports, i,j in comps], 
                first_fail, indomain_max, complete)
              ]) """,],
  }

DEFAULT_SOLVER = [ 'lex-chuffed' ]

# add symmetry breaking constraints
SIMMETRY_BREAKING = True

# linearization seems to help a lot (more than twice faster)
# it messes however with minisearch that relies on gecode (probably
# the reification triggers a mzn2fzn error when gecode is used).
# use minisearch with or-tools instead

MZN_PROGRAM = 'minzinc_prog_with_linearization.mzn'
#MZN_PROGRAM = 'minzinc_prog.mzn'

def usage():
  """Print usage"""
  print(__doc__)
  print "The list of the supported solvers is: " + str.join(', ',SOLVERS.keys())
  print "The default solver is " + DEFAULT_SOLVER[0]

def handler(signum = None, frame = None):
  """
  Handles termination signals.
  """
  log.critical('Signal handler called with signal' + str(signum))
  clean()
  sys.stdout.close()
  sys.exit(signum)

for sig in [signal.SIGTERM, signal.SIGINT, signal.SIGHUP, signal.SIGQUIT]:
  signal.signal(sig, handler)

 
def clean():
  """
  Utility for (possibly) cleaning temporary files and killing the solvers 
  processes at the end of the solving process (even when the termination is 
  forced externally).
  """
  global RUNNING_SOLVERS
  for solver in RUNNING_SOLVERS:
    solver.terminate()
  # Possibly remove temporary files.
  if not KEEP:
    settings.TMP_FILES_LOCK.acquire()
    for f in settings.TMP_FILES:
      if os.path.exists(f):
        os.remove(f)
    settings.TMP_FILES_LOCK.release()

def read_json(json_file): 
  json_data = open(json_file)
  data = json.load(json_data)
  json_data.close()
  return data


def get_ports(data):
  """Extract the names of the ports"""
  ports = set([])
  for i in data["components"].keys():
    for j in data["components"][i]["provides"]:
      for k in j["ports"]:
        ports.add(k)
    for j in data["components"][i]["requires"].keys():
        ports.add(j)
        
  int_to_port = {}
  port_to_int = {}
  counter = 1
  for i in ports:
    int_to_port[counter] = i
    port_to_int[i] = counter
    counter += 1
  return (int_to_port,port_to_int)


def get_comps(data):
  comp_to_int = {}
  int_to_comp = {}
  counter = 1
  for i in data["components"].keys():
    int_to_comp[counter] = i
    comp_to_int[i] = counter
    counter += 1
  return (int_to_comp,comp_to_int)


def get_multiple_provs(data,comp_to_int,port_to_int):
  mp =  {}
  counter = 1
  for i in data["components"].keys():
    for j in data["components"][i]["provides"]:
      mp[counter] = (
        map(lambda x: port_to_int[x], j["ports"]),
        j["num"],
        comp_to_int[i])
      counter += 1
  return mp


def get_locations(data):
  loc_to_int = {}
  int_to_loc = {}
  counter = 1
  for i in data["locations"].keys():
    for j in range(data["locations"][i]["num"]):
      int_to_loc[counter] = (i,j)
      loc_to_int[(i,j)] = counter
      counter += 1
  return (int_to_loc,loc_to_int)
  
    
def get_resources(data):  
  res = set([])
  for i in data["components"].keys():
    res = res.union(set(data["components"][i]["resources"].keys()))
  for i in data["locations"].keys():
    res = res.union(set(data["locations"][i]["resources"].keys()))
  
  res_to_int = {}
  int_to_res = {}
  counter = 1
  for i in res:
    res_to_int[i] = counter
    int_to_res[counter] = i
    counter += 1
  return (int_to_res,res_to_int)



def print_dzn_matrix(ls,n,out):
  ls.reverse()
  out.write("[|\n")
  while ls:
    for _ in range(n-1):
      out.write(ls.pop() + ",")
    out.write(ls.pop() + "|")
  out.write("];\n") 
              

def generate_dzn(data,
               int_to_port, port_to_int,
               int_to_comp, comp_to_int,
               int_to_loc, loc_to_int,
               int_to_res, res_to_int,
               mports,
               out):
  """Generate the dzn"""
  
  out.write("comps = 1.." + str(len(int_to_comp)) + ";\n")
  out.write("ports = 1.." + str(len(int_to_port)) + ";\n")
  out.write("multi_provide_ports = 1.." + str(len(mports)) + ";\n")
  
  out.write("locations = 1.." + str(len(loc_to_int)) + ";\n")
  out.write("resources = 1.." + str(len(res_to_int)) + ";\n")
  
  out.write("requirement_port_nums = ")
  temp = []
  for i in range(1,len(int_to_comp)+1):
    for j in range(1,len(int_to_port)+1):
      if int_to_port[j] in data["components"][int_to_comp[i]]["requires"]:
        temp.append(str(data["components"][int_to_comp[i]]["requires"][int_to_port[j]]))
      else:
        temp.append("0")
  print_dzn_matrix(temp,len(int_to_port), out)
  
  out.write("provide_port_nums = ") 
  temp = {}
  for i in range(1,len(int_to_comp)+1):
    temp[i] = {}
    for j in range(1,len(mports)+1):
      temp[i][j] = "0"
  for i in mports.keys():    
      (_,x,y) = mports[i]
      temp[y][i] = str(x)             
  print_dzn_matrix(
    [ temp[i][j] for i in range(1,len(int_to_comp)+1) for j in range(1,len(mports)+1)],
    len(mports), out)

  out.write("conflicts = ")
  temp = {}
  for i in range(1,len(comp_to_int)+1):
    temp[i] = {}
    for j in range(1,len(port_to_int)+1):
      temp[i][j] = "false"      
  for i in data["components"].keys():
    for j in data["components"][i]["conflicts"]:
      temp[comp_to_int[i]][port_to_int[j]] = "true"
  print_dzn_matrix(
    [ temp[i][j] for i in range(1,len(int_to_comp)+1) for j in range(1,len(int_to_port)+1)],
    len(int_to_port), out)
  
  out.write("multi_provides = ")
  temp = []
  for i in range(1,len(mports)+1):
    for j in range(1,len(int_to_port)+1):
      if j in mports[i][0]:
        temp.append("true")
      else:
        temp.append("false")
  print_dzn_matrix( temp, len(int_to_port), out)

  out.write("costs = [")
  out.write(str(data["locations"][int_to_loc[1][0]]["cost"]))
  for i in range(2,len(int_to_loc)+1):
    out.write(", " + str(data["locations"][int_to_loc[i][0]]["cost"]))
  out.write("];\n")
  
  out.write("resource_provisions = ")
  temp = []
  for i in range(1,len(int_to_loc)+1):
    for j in  range(1,len(int_to_res)+1):
      if int_to_res[j] in data["locations"][int_to_loc[i][0]]["resources"]:
        temp.append(str(data["locations"][int_to_loc[i][0]]["resources"][int_to_res[j]]))
      else:
        temp.append("0")
  print_dzn_matrix( temp, len(int_to_res), out)

  out.write("resource_consumptions = ")
  temp = []
  for i in range(1,len(int_to_comp)+1):
    for j in  range(1,len(int_to_res)+1):
      if int_to_res[j] in data["components"][int_to_comp[i]]["resources"]:
        temp.append(str(data["components"][int_to_comp[i]]["resources"][int_to_res[j]]))
      else:
        temp.append("0")
  print_dzn_matrix( temp, len(int_to_res), out)


def is_solution_better(xs,ys):
  if ys == None:
    return True
  for i in range(len(xs)):
    if xs[i] > ys[i]:
      return False
  return True


def print_solution(best_sol, buf,
               int_to_port, port_to_int,
               int_to_comp, comp_to_int,
               int_to_loc, loc_to_int,
               int_to_res, res_to_int,
               mports,
               out):
  lines = buf.split("\n")
  bindings = []
  loc_comp = []
  for line in lines:
    if line.startswith("bindings|"):
      bindings = line.split("|")[1][1:-1].split(",")
    elif line.startswith("comp_locations|"):
      loc_comp = line.split("|")[1][1:-1].split(",")
    elif line.startswith("###OBJVAR"):
      obj_array = map(lambda x: (int)(x), line.split("|")[1][1:-1].split(","))
      if not is_solution_better(obj_array,best_sol):
        return best_sol
  
  log.debug('Find better solution with obj value: ' + str(obj_array)) 
    
  data_loc = {}
  
  for i in range(len(int_to_loc)):
    for j in range(len(int_to_comp)):
      obj = (int)(loc_comp[i*len(int_to_comp) + j])
      if obj > 0:
        (comp, num) = int_to_loc[i+1]
        if comp not in data_loc:
          data_loc[comp] = {}
        if str(num) not in data_loc[comp]:
          data_loc[comp][str(num)] = {}
        data_loc[comp][str(num)][int_to_comp[j+1]] = obj  
  
  data_bind = []
  counter = 0
  for i in map( lambda x: (int)(x), bindings):
    if i > 0:
      req = counter % len(int_to_comp)
      prov = (counter - req) / len(int_to_comp) % len(int_to_comp)
      port = ((counter - req) / len(int_to_comp) - prov) / len(int_to_comp) % len(int_to_port)
      data_bind.append({
                   "provider": int_to_comp[prov+1],
                   "requirer": int_to_comp[req+1],
                   "port": int_to_port[port+1],
                   "num": i })
    counter += 1
  
  data = { "locations" : data_loc, "bindings" : data_bind } 
  json.dump(data,out,indent=1)
  out.write("\n----------\n")
  return obj_array


def main(argv):
  """Main procedure """   
  output_file = ""
  
      
  global SOLVERS
  global DEFAULT_SOLVER
  try:
    opts, args = getopt.getopt(argv,"ho:vckG:f:s:S",
      ["help","ofile","verbose","check-sol","keep", 'globals-dir',
       'flatzinc-cmd', 'solver', 'no-simmetry-breaking'])
  except getopt.GetoptError as err:
    print str(err)
    usage()
    sys.exit(1)
  for opt, arg in opts:
    if opt == '-h':
      usage()
      sys.exit()
    elif opt in ("-o", "--ofile"):
      output_file = arg
    elif opt in ("-c", "--check-sol"):
      global CHECK_SOLUTION
      CHECK_SOLUTION = True
    elif opt in ("-k", "--keep"):
      global KEEP
      KEEP = True
    elif opt in ('-S', '--no-simmetry-breaking'):
      global SIMMETRY_BREAKING
      SIMMETRY_BREAKING = False
    elif opt in ('-G', '--globals-dir'):
      for i in DEFAULT_SOLVER:
        SOLVERS[i][2]['--globals-dir'] = arg
    elif opt in ('-f', '--flatzinc-cmd'):
      for i in DEFAULT_SOLVER:
        SOLVERS[i][2]['--solver'] = arg
    elif opt in ('-s', '--solver'):
      solvers = arg.split(',')
      for i in solvers:
        if not i in SOLVERS:
          print "solver " + arg + "not found"
          usage()
          sys.exit(1)
      DEFAULT_SOLVER = solvers
    elif opt in ("-v", "--verbose"):
      log.basicConfig(format="%(levelname)s: %(message)s", level=log.DEBUG)
      log.info("Verbose output.")
  
  if len(args) != 1:
    print "1 argument is required"
    usage()
    sys.exit(1)
    
  input_file = args[0]
  out_stream = sys.stdout
  if output_file:
    out_stream = open(output_file, "w") 
   
  script_directory = os.path.dirname(os.path.realpath(__file__))
   
  input_file = os.path.abspath(input_file)
  
  mzn_file = settings.get_new_temp_file("mzn")
  dzn_file = settings.get_new_temp_file("dzn")
  
  log.info("Parsing JSON file")
  try:
    data = settings.validate_input_json(read_json(input_file))

    (int_to_port, port_to_int) = get_ports(data)
    (int_to_comp,comp_to_int) = get_comps(data)
    (int_to_loc,loc_to_int) = get_locations(data)
    (int_to_res,res_to_int) = get_resources(data)
    mports = get_multiple_provs(data,comp_to_int,port_to_int)


    log.debug("components ids:")
    log.debug(str(comp_to_int))
    log.debug("ports ids:")
    log.debug(str(port_to_int))
    log.debug("locations ids:")
    log.debug(str(loc_to_int))
    log.debug("resources ids:")
    log.debug(str(res_to_int))

    log.info("Generating DZN")

    with open(dzn_file, 'w') as f:
      generate_dzn(data,
                 int_to_port, port_to_int,
                 int_to_comp, comp_to_int,
                 int_to_loc, loc_to_int,
                 int_to_res, res_to_int,
                 mports,
                 f)

    log.debug("****DZN****:")
    with open(dzn_file, 'r') as f:
      for i in f.readlines():
        log.debug(i[:-1])
    log.debug("****DZN****:")

    shutil.copyfile(script_directory + "/" + MZN_PROGRAM, mzn_file)


    log.info("Processing specification")
    try:
      spec = spec_translator.translate_specification(
              InputStream(data["specification"]), comp_to_int, res_to_int,
              loc_to_int, SIMMETRY_BREAKING)
    except spec_translator.SpecificationParsingException as e:
      log.critical("Parsing of the specification failed: " + e.value)
      log.critical("Exiting")
      sys.exit(1)

    with open(mzn_file, 'a') as f:
      f.write(spec)

    log.debug("****Specification****")
    log.debug(spec)
    log.debug("****Specification****")

    log.info("Running solvers")

    global RUNNING_SOLVERS
    RUNNING_SOLVERS = [
      Solver(
        SOLVERS[i][0],
        mzn_file, dzn_file,
        SOLVERS[i][1],
        i,
        options = [ item for pair in SOLVERS[i][2].items() for item in pair ],
        search_annotation = SOLVERS[i][3]
        ) for i in DEFAULT_SOLVER ]

    for i in RUNNING_SOLVERS:
      i.start()
      log.debug("Solver " + i.get_id() + " started")

    ended = False
    best_solution = None
    while not ended and RUNNING_SOLVERS:
      time.sleep(settings.SLEEP_TIME)
      #log.debug('Checking if new soutions have arrived')
      for i in RUNNING_SOLVERS:
        sol = i.get_solution()
        if sol != "":
          log.debug("solution found by " + i.get_id())
          log.debug(sol)
          best_solution = print_solution(best_solution, sol,
                int_to_port, port_to_int,
                int_to_comp, comp_to_int,
                int_to_loc, loc_to_int,
                int_to_res, res_to_int,
                mports,
                out_stream)
        if i.is_ended():
          if i.process.returncode == 0:
            log.info("Search completed by " + i.get_id())
            out_stream.write("\n==========\n")
            RUNNING_SOLVERS.remove(i)
            ended = True
            break
          else:
            log.info("Solver " + i.get_id() + " terminated with code " +
                     str(i.process.returncode))
            RUNNING_SOLVERS.remove(i)

    if not ended:
      log.debug('All the running solvers ended up in error')
      log.debug('Cleaning end exiting with code 1')
      clean()
      sys.exit(1)

    if CHECK_SOLUTION:
      log.info("Checking the optimality of the solution")
      if best_solution:
        with open(mzn_file, 'a') as outfile:
          outfile.write('constraint lex_less(obj_array,')
          outfile.write(str(best_solution) + ');')

      solv_checker = Solver("minizinc", mzn_file, dzn_file, 'metric_satisfy', "gecode",
                 options = ['--flatzinc-cmd', '"fzn-gecode"'])
      RUNNING_SOLVERS.append(solv_checker)
      solv_checker.run()
      log.debug("Solver " + i.get_id() + " started")
      out = solv_checker.get_solution()
      if out:
        log.info("Optima proven to be correct")
      else:
        log.critical("ERROR: the solution is not optimal, a solver may be wrong")
        log.critical("Exiting")
        clean()
        sys.exit(1)

    log.info("Clean.")
    clean()
    log.info("Program Succesfully Ended")

  except ValueError as e:
    log.error("{}. Exiting".format(e))
    sys.exit(1)


if __name__ == "__main__":
  main(sys.argv[1:])