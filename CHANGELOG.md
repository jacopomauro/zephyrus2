# Zephyrus2 CHANGELOG

#### v2.0.1
Minor bugs and changes:
* Docker -> switch to use other docker images (faster Docker image generation)
* Removed the code to use Minisearch
* Add the possibility to use the or-tools (gold winner of MiniZinc Challenge 2018)
* chuffed is the new default solver to use when used only Zephyrus

#### v2.0.0

Major changes:
* Output of the binding optimizer and Zephyrus configuratio is merged in the output

Minor changes:
* small bux fixes
* update readme

#### v1.1

Minor bugs and changes:
* Small updates on Dockerfile
* Add wrapper to run Zephyrus and the binder optimizer

