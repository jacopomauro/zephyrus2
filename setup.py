from setuptools import setup

setup(name='zephyrus2',
      version='0.1',
      description='python meet zephyrus',
      url='toadd',
      author='Jacopo Mauro',
      author_email='mauro.jacopo@gmail.com',
      license='GNU GPL',
      packages=['zephyrus2'],
      zip_safe=False)